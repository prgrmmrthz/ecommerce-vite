import { Container, Typography } from '@mui/material'
import React from 'react'

export default function ServerError() {
    return (
        <Container>
            <Typography gutterBottom variant='h5'>Server error</Typography>
        </Container>
    )
}
