import { Button, Container, Divider, Paper, Typography } from '@mui/material'
import React from 'react'
import { Link } from 'react-router-dom'

export default function NotFound() {
    return (
        <Container component={Paper} sx={{height: 400}}>
            <Typography gutterBottom variant='h5'>Error 404 Not Found</Typography>
            <Divider />
            <Button fullWidth component={Link} to="/catalog">Go back to Shop</Button>
        </Container>
    )
}
