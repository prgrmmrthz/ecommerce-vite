export interface FamBG {
    id: number;
    title: string | null;
    name: string | null;
    age: number | null;
    birthday: string | null;
    birthPlace: string | null;
    nationality: string | null;
    religion: string | null;
    educationalAttainment: string | null;
    occupation: string | null;
    nameAddressOfCompany: string | null;
    contactNumber: number | null;
    email: string | null;
  }

export interface SiblingSchema{
  id: number;
  name: string;
  sex: string;
  age: number | null;
  civilStatus: string;
  schoolOrOccupation: string;
  gradeyearCompanyOrFirm: string;
}

export interface EducationalBGSchema{
  id: number;
  level: string;
  school: string;
  address: string;
  yearsAttended: string;
  honors: string;
}