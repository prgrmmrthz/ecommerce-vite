export interface Product {
    id: number
    name: string
    barcode?: string
    price: number
    member_price?: number
    class_id?: number
    classification?: string
    pictureUrl: string
    description: string
    type: string
    brand: string
    quantityInStock: number
}
