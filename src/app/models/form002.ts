import { EducationalBGSchema, SiblingSchema } from "./famBG";

export interface CreateForm002Schema {
    academicYear: string;
    fatherName?: string | null;
    fatherPlaceofBirth?: string | null;
    fatherNationality?: string | null;
    fatherReligion?: string | null;
    fatherEducation?: string | null;
    fatherOccupation?: string | null;
    fatherCompanyNameAddress?: string | null;
    fatherContactNumber?: string | null;
    fatherEmail?: string | null;
    fatherAge?: number | null;
    fatherDOB?: string | null;
    motherName?: string | null;
    motherPlaceofBirth?: string | null;
    motherNationality?: string | null;
    motherReligion?: string | null;
    motherEducation?: string | null;
    motherOccupation?: string | null;
    motherCompanyNameAddress?: string | null;
    motherContactNumber?: string | null;
    motherEmail?: string | null;
    motherAge?: number | null;
    motherDOB?: string | null;
    guardianName?: string | null;
    guardianPlaceofBirth?: string | null;
    guardianNationality?: string | null;
    guardianReligion?: string | null;
    guardianEducation?: string | null;
    guardianOccupation?: string | null;
    guardianCompanyNameAddress?: string | null;
    guardianContactNumber?: string | null;
    guardianEmail?: string | null;
    guardianAge?: number | null;
    guardianDOB?: string | null;
    parentsMaritalStatus: string;
    numberOfRelativesAtHome: number;
    numberOfHelpersAtHome: number;
    numberOfMaleAtHome: number;
    numberOfFemaleAtHome: number;
    healthInformationList?: string[];
    healthInformationListOthers?: OtherHealthInfoSchema;
    generalPersonalityMakeupList: string[];
    generalPersonalityMakeupListOthers: string;
    socialRelationshipList: string[];
    socialRelationshipListOthers: string;
    atHomeOurChildToBe: string;
    thingsMakeAngry: string;
    talentsHobby: string;
    strongPoints: string;
    weakness: string;
    commentsSuggestions: string;
    siblings: SiblingSchema[];
    educationalBackgrounds: EducationalBGSchema[];
}

export interface PersonalData{
    lastName: string;
    firstName: string;
    middleInitial: string;
    nickname: string;
    gradeYearLevel: string;
    section: string;
    age: number;
    birthday: Date | null;
    placeOfBirth?: any;
    nationality?: any;
    religion: string;
    email?: any;
    address?: any;
    contactNumber: number;
    studentCode: number;
    academicYear: string;
    gender: string
}

export interface FamilyBackground{
    Frm002Id: number;
    fatherName?: string | null;
    fatherPlaceofBirth?: string | null;
    fatherNationality?: string | null;
    fatherReligion?: string | null;
    fatherEducation?: string | null;
    fatherOccupation?: string | null;
    fatherCompanyNameAddress?: string | null;
    fatherContactNumber?: number | null;
    fatherEmail?: string | null;
    fatherAge?: number | null;
    fatherDOB?: string | null;
    motherName?: string | null;
    motherPlaceofBirth?: string | null;
    motherNationality?: string | null;
    motherReligion?: string | null;
    motherEducation?: string | null;
    motherOccupation?: string | null;
    motherCompanyNameAddress?: string | null;
    motherContactNumber?: number | null;
    motherEmail?: string | null;
    motherAge?: number | null;
    motherDOB?: string | null;
    guardianName?: string | null;
    guardianPlaceofBirth?: string | null;
    guardianNationality?: string | null;
    guardianReligion?: string | null;
    guardianEducation?: string | null;
    guardianOccupation?: string | null;
    guardianCompanyNameAddress?: string | null;
    guardianContactNumber?: number | null;
    guardianEmail?: string | null;
    guardianAge?: number | null;
    guardianDOB?: string | null;
    parentsMaritalStatus: string;
}

export interface Sibling {
    name: string
    sex: string
    age: number
    civilStatus: string
    shooolOrOccupation: string
    gradeYearCompanyFirm: string
}

export interface EducationalBackground {
    level: string
    school: string
    address: string
    yearsAttended: string
    honorsAwards: string
}

export interface OtherHealthInfoSchema {
    previousHospitalization: string;
    previousHospitalizationDate: string;
    specifyOperation: string;
    specifyOperationDate: string;
    pastMedicationsGiven: string;
    foodAllergies: string;
    drugsAllergies: string;
    otherHealthMedications: string;
    IndicatedCondition: string;
    schoolMedication: string;
    professionalname: string;
    professionalWhere: string;
    professionalWhen: string;
    professionalDuration: string;
    professionalConcern: string;
}