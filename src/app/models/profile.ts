interface ProfileSchema {
    id: number;
    lastName: string;
    firstName: string;
    middleInitial: string;
    nickname: string;
    gradeYearLevel: string;
    section: string;
    age: number;
    birthday: Date | null;
    placeOfBirth?: any;
    nationality?: any;
    religion: string;
    email?: any;
    address?: any;
    contactNumber: number;
    form002?: any;
    pictureString?: any;
    citizenship: string;
    studentCode: number;
  }