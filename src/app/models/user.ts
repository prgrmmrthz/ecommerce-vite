export interface User{
    email: string;
    token: string;
    username: string;
    studentCode: number;
    fullName: string;
    roles: string[];
    profPic: File;
}

export interface UserProfile{
    savedCards: number;
    name?: string;
    coverImg: string;
    avatar: string;
    description: string;
    nikcName: string;
    gradeSection: string;
    birthday: string;
}

export interface UserFormFields{
    name: string;
    label: string;
    sx: number;
    Lsx: number | 'auto';
    type: 'text' | 'number' | 'date' | 'email'
}