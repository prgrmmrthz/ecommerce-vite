import { Navigate, createBrowserRouter } from "react-router-dom";
import App from "../layout/App";
import React from "react";
import Catalog from "../../features/catalog/Catalog";
import ProductDetails from "../../features/catalog/productDetails";
import AboutPage from "../../features/About/AboutPage";
import ServerError from "../errors/ServerError";
import NotFound from "../errors/NotFound";
import BasketPage from "../../features/basket/BasketPage";
import CheckoutPage from "../../features/checkout/CheckoutPage";
import Login from "../../features/account/Login";
import InterviewForm003 from "../../features/interview";
import Dashboard from "../layout/Dashboard";
import Form001 from "../../features/form-001/Form001";
import StudentIndividualForm from "../../features/student-individual-form/StudentIndividualForm";
import InterviewFormListMain from "../../features/interview/InterviewFormListMain";
import ManagementUserProfile from "src/content/applications/Users/profile";
import Form001List from "src/features/form-001/Form001List";

export const router = createBrowserRouter([
    {
        path: '',
        element: <App />,
        children: [
            {path: '', element: <Dashboard /> },
            {path: 'catalog', element: <Catalog /> },
            {path: 'catalog/:id', element: <ProductDetails /> },
            {path: 'about', element: <AboutPage /> },
            {path: 'dashboard', element: <Dashboard /> },
            {path: 'server-error', element: <ServerError /> },
            {path: 'not-found', element: <NotFound /> },
            {path: 'basket', element: <BasketPage /> },
            {path: 'checkout', element: <CheckoutPage /> },
            {path: 'login', element: <Login /> },
            {path: 'interview', element: <InterviewForm003 /> },
            {path: 'form-001', element: <Form001 /> },
            {path: 'form-001-list', element: <Form001List /> },
            {path: 'interview-list', element: <InterviewFormListMain /> },
            {path: 'individual-form', element: <StudentIndividualForm /> },
            {path: 'user-profile', element: <ManagementUserProfile /> },
            {path: '*', element: <Navigate replace to='/not-found' /> },
        ]
    }
]);