import React, { useState, useEffect } from "react";
import { CssBaseline, createTheme } from "@mui/material";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import LoadingComponent from "./LoadingComponent";
import { useAppDispatch, useAppSelector } from "../store/configureStore";
import { fetchCurrentUser } from "../../features/account/accountSlice";
import { router } from "../router/Routes";
import DrawerMain from "./DrawerMain";
import ThemeProvider from '../../theme/ThemeProvider';
import SidebarLayout from "../../layouts/SidebarLayout";

function App() {
  const { user } = useAppSelector(state => state.account);
  const dispatch = useAppDispatch();
  const [loading, setloading] = useState(true);

  useEffect(() => {
    //const buyerId = getCookie('buyerId');
    //dispatch(fetchCurrentUser());
    if (user) {
      dispatch(fetchCurrentUser());
    }else{
      router.navigate('/login');
    }
    setloading(false);
    //router.navigate('/login');

    /* if (buyerId) {
      agent.Basket.get()
        .then(basket => dispatch(setBasket(basket)))
        .catch(err => console.log(err))
        .finally(() => setloading(false))
    } else {
      setloading(false);
    } */
  }, [dispatch])

/*   const [darkMode] = useState(false);
  const paletteType = darkMode ? "dark" : "light";
  const theme = createTheme({
    palette: {
      mode: paletteType,
      background: {
        default: (paletteType === 'light') ? "#eaeaea" : '#121212'
      },
    },
  }); */


  if (loading)
    return <LoadingComponent message="Initializing App..."></LoadingComponent>;

  return (
    <ThemeProvider>
      <ToastContainer position="bottom-right" hideProgressBar theme="colored" />
      <CssBaseline />
      {/* {user && <Header darkMode={darkMode} handleThemeChange={handleThemeChange} />} */}
      {/* <DrawerMain /> */}
      {/* <Container>
      
        <Outlet />
      </Container> */}
      <SidebarLayout />
    </ThemeProvider>
  );
}

export default App;

