import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import LayersIcon from '@mui/icons-material/Layers';
import { NavLink } from 'react-router-dom';
import { useAppSelector } from '../store/configureStore';
import { List } from '@mui/material';


export default function ListItemsAdmin() {
  const { user } = useAppSelector(state => state.account);

  const listOfForms = [
    { id: 100, title: 'Dashboard', link: '/dashboard' },
    { id: 1, title: 'Initial Interview Form', link: user?.roles[0] === 'Admin' ? '/interview-list' : '/interview' },
    { id: 2, title: 'Confidentiality Agreement Form', link: '/' },
    { id: 3, title: 'Student individual Inventory Form', link: '/' },
    { id: 4, title: 'Routine Interview Form', link: '/' },
    { id: 5, title: 'PDF 007 - Intake Interview Form', link: '/' },
    { id: 6, title: '007A - Intake Summary Form', link: '/' },
    { id: 7, title: 'PDF 008 - Informed Consent Form', link: '/' },
    { id: 8, title: '009 - Progress Report', link: '/' },
    { id: 9, title: '011 - Parent Conference Form ', link: '/' },
    { id: 10, title: '011A - Parent Acknowledgement Form', link: '/' },
    { id: 11, title: '012 Closing Summary Form', link: '/' },
    { id: 12, title: '013- Closing Case Checklist', link: '/' },
    { id: 13, title: '015 - Referral Form for External Professional', link: '/' },
    { id: 14, title: '016 - Request for Release of Informtion Form', link: '/' },
    { id: 15, title: '018 - Academic Referral and Counseling Form}', link: '/' },
    { id: 16, title: '019 - Personality Development Report ', link: '/' },
    { id: 17, title: '021 - Non-Counseling Contact Form', link: '/' }
  ]

  if (!user){
    return false;
  }
  return (
    <List>
      {listOfForms.map((item) => (
        <ListItemButton dense component={NavLink} to={item.link} key={item.id}>
          <ListItemIcon>
            <LayersIcon />
          </ListItemIcon>
          <ListItemText primary={item.title} />
        </ListItemButton>
      ))}
    </List>
  );

}

