import { Link, Typography } from '@mui/material'
import React from 'react'
import Title from './Title';

function preventDefault(event: React.MouseEvent) {
    event.preventDefault();
}

interface Props {
    title: string;
    subTitle: string;
}
  

export default function CardDashboard({title, subTitle}: Props) {
  return (
    <React.Fragment>
      <Title>{title}</Title>
      <Typography component="p" variant="h4">
        {subTitle}
      </Typography>
      <Typography color="text.secondary" sx={{ flex: 1 }}>
        on 15 March, 2019
      </Typography>
      <div>
        <Link color="primary" href="#" onClick={preventDefault}>
          View
        </Link>
      </div>
    </React.Fragment>
  )
}
