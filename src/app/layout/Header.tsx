import {
  Box,
  AppBar,
  Toolbar,
  Typography,
  List,
  ListItem,
  IconButton,
  Switch,
  Drawer,
} from "@mui/material";
import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { useAppSelector } from "../store/configureStore";
import SignedInMenu from "./SignedInMenu";
import MenuIcon from '@mui/icons-material/Menu';
import ListItemsAdmin from "./listItemsAdmin";


const rightLinks = [
  { title: "login", path: "/login" },
  { title: "register", path: "/register" },
];

interface Props {
  darkMode: boolean;
  handleThemeChange: () => void;
}

const navStyles = {
  color: "inherit",
  textDecoration: "none",
  typography: "h6",
  "&:hover": {
    color: "grey.500",
  },
  "&.active": {
    color: "text.secondary",
  },
};

export default function Header({ darkMode, handleThemeChange }: Props) {
  const { user } = useAppSelector(state => state.account);
  //const itemCount = basket?.items.reduce((sum, item) => sum + item.quantity, 0);

  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = (open: any) => (event: any) => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setDrawerOpen(open);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" sx={{ mb: 4 }}>
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        ><Box display='flex' alignItems='centers'>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={toggleDrawer(true)}
            >
              <MenuIcon />
            </IconButton>

            <Typography variant="h6" component={NavLink} to="/" sx={navStyles}>
              App
            </Typography>
            <Switch checked={darkMode} onChange={handleThemeChange} />
          </Box>

          <List sx={{ display: "flex" }}>

            <ListItem component={NavLink} to='/about' sx={navStyles}>
              About
            </ListItem>
          </List>

          <Box display='flex' alignItems='centers'>
            {/* <IconButton component={NavLink} to="/basket" edge="start" color="inherit" sx={{ mr: 2 }}>
              <Badge badgeContent={itemCount} color="secondary">
                <ShoppingCart />
              </Badge>
            </IconButton> */}
            {user ? (
              <SignedInMenu />
            ) : (<List sx={{ display: "flex" }}>
              {rightLinks.map(({ title, path }) => (
                <ListItem
                  component={NavLink}
                  to={path}
                  key={path}
                  sx={navStyles}
                >
                  {title.toUpperCase()}
                </ListItem>
              ))}
            </List>)}

          </Box>
        </Toolbar>
      </AppBar>
      <Drawer
        anchor="left"
        open={drawerOpen}
        onClose={toggleDrawer(false)}
      >
        <ListItemsAdmin />
      </Drawer>
    </Box>
  );
}
