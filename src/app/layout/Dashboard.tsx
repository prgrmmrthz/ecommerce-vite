import { Grid, Paper } from '@mui/material'
import React from 'react'
import CardDashboard from './CardDashboard'

export default function Dashboard() {
  const a = {
    title: 'Initial Interview Form',
    count: 40
  }
  return (
    <Grid container spacing={3}>
      {/* Chart */}
      {/* Recent Deposits */}
      <Grid item xs={12} md={4} lg={3}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240,
          }}
        >
          <CardDashboard title="Confidentiality Agreement Form" subTitle={'Filed:' + 18} />
        </Paper>
      </Grid>
      <Grid item xs={12} md={4} lg={3}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240,
          }}
        >
          <CardDashboard title='Student Individual Inventory' subTitle={'Filed:' + 30} />
        </Paper>
      </Grid>
      <Grid item xs={12} md={4} lg={3}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240,
          }}
        >
          <CardDashboard title={a.title} subTitle={'Filed:' + a.count} />
        </Paper>
      </Grid>
      {/* Recent Orders */}
    </Grid>
  )
}
