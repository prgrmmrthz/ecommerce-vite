import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import { useAppSelector } from "../store/configureStore";
import { NavLink, Outlet } from 'react-router-dom';
import { Grid } from '@mui/material';
import SignedInMenu from './SignedInMenu';

const rightLinks = [
  { title: "login", path: "/login" },
  { title: "register", path: "/register" },
];

const drawerWidth = 300;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  padding: theme.spacing(3),
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginLeft: `-${drawerWidth}px`,
  ...(open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  }),
}));

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const navStyles = {
  color: "inherit",
  textDecoration: "none",
  typography: "h6",
  "&:hover": {
    color: "grey.500",
  },
  "&.active": {
    color: "text.secondary",
  },
};

export default function DrawerMain() {
  const { user } = useAppSelector(state => state.account);
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  //const user = { roles: 'Admin' };
  const listOfForms = [
    { id: 0, title: 'Dashboard', link: '/dashboard' },
    { id: 1, title: 'Initial Interview Form', link: user?.roles[0] === 'Admin' ? '/interview-list' : '/interview' },
    { id: 2, title: '001 - Confidentiality Agreement Form', link: '/form-001' },
    { id: 3, title: 'Student individual Inventory Form', link: user?.roles[0] === 'Admin' ? '/individual-list' : '/individual-form' },
    { id: 4, title: 'Routine Interview Form', link: '/' },
    { id: 5, title: 'PDF 007 - Intake Interview Form', link: '/' },
    { id: 6, title: '007A - Intake Summary Form', link: '/' },
    { id: 7, title: 'PDF 008 - Informed Consent Form', link: '/' },
    { id: 8, title: '009 - Progress Report', link: '/' },
    { id: 9, title: '011 - Parent Conference Form ', link: '/' },
    { id: 10, title: '011A - Parent Acknowledgement Form', link: '/' },
    { id: 11, title: '012 Closing Summary Form', link: '/' },
    { id: 12, title: '013- Closing Case Checklist', link: '/' },
    { id: 13, title: '015 - Referral Form for External Professional', link: '/' },
    { id: 14, title: '016 - Request for Release of Informtion Form', link: '/' },
    { id: 15, title: '018 - Academic Referral and Counseling Form', link: '/' },
    { id: 16, title: '019 - Personality Development Report ', link: '/' },
    { id: 17, title: '021 - Non-Counseling Contact Form', link: '/' }
  ]

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        {user && (<Toolbar sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}>
          <Box display='flex' alignItems='center'>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              sx={{ mr: 2, ...(open && { display: 'none' }) }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap component="div">
              SPCM FORMS
            </Typography>
          </Box>

          <Box display='flex' alignItems='center' alignSelf='right'>
            {/* <IconButton component={NavLink} to="/basket" edge="start" color="inherit" sx={{ mr: 2 }}>
              <Badge badgeContent={itemCount} color="secondary">
                <ShoppingCart />
              </Badge>
            </IconButton> */}
            {user ? (
              <SignedInMenu />
            ) : (<List sx={{ display: "flex" }}>
              {rightLinks.map(({ title, path }) => (
                <ListItem
                  component={NavLink}
                  to={path}
                  key={path}
                  sx={navStyles}
                >
                  {title.toUpperCase()}
                </ListItem>
              ))}
            </List>)}

          </Box>
        </Toolbar>)}
      </AppBar>
      {user && (<Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          {listOfForms.map((text, index) => (
            <ListItem key={text.id} disablePadding>
              <ListItemButton component={NavLink} to={text.link}>
                <ListItemIcon>
                  {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                </ListItemIcon>
                <ListItemText primaryTypographyProps={{fontSize: '12px'}} primary={text.title}  />
              </ListItemButton>
            </ListItem>
          ))}
        </List>

        <Divider />
      </Drawer>)}
      <Main open={open}>
        <DrawerHeader />
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justifyContent="center"
        >

          <Outlet />
        </Grid>
      </Main>
    </Box>
  );
}
