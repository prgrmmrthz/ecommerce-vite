import { configureStore } from "@reduxjs/toolkit";
import { counterSlice } from "../../features/About/counterSlice";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { basketSlice } from "../../features/basket/basketSlice";
import { catalogSlice } from "../../features/catalog/catalogSlice";
import { accountSlice } from "../../features/account/accountSlice";
import { interviewFormSlice } from "../../features/interview/interviewFormSlice";
import { formFieldSlice } from "../../features/student-individual-form/formFieldSlice";

/* export function configureStore(){
    return createStore(counterReducer);
} */

export const store = configureStore({
    reducer: {
        counter: counterSlice.reducer,
        basket: basketSlice.reducer,
        catalog: catalogSlice.reducer,
        account: accountSlice.reducer,
        interviewForm: interviewFormSlice.reducer,
        formField: formFieldSlice.reducer
    }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;