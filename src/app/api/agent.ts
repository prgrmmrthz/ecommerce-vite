import axios, { AxiosError, AxiosResponse } from "axios";
import { toast } from "react-toastify";
import { store } from "../store/configureStore";

const sleep = () => new Promise(resolve => setTimeout(resolve, 500));

//console.log(import.meta.env.VITE_API_URL);
export const apiURL = import.meta.env.VITE_API_URL;
axios.defaults.baseURL = apiURL;
axios.defaults.withCredentials = true;

const responseBody = (response: AxiosResponse) => response.data;

axios.interceptors.request.use(config => {
    const token = store.getState().account.user?.token;
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
});

axios.interceptors.response.use(async response => {
    await sleep();
    return response
}, (error: AxiosError) => {
    const {data, status, statusText} = error.response as AxiosResponse;
    switch(status){
        case 400:
            toast.error(data.title + JSON.stringify(data.errors));
            break;
        case 500:
            //router.navigate('/server-error');
            break;
        case 404:
            toast.error(data.title);
            break;
        case 401:
                toast.error(statusText);
            break;
        default:
            break;
    }
    return Promise.reject(error.response);
});

const request = {
    get: (url: string) => axios.get(url).then(responseBody),
    post: (url: string, body: object) => axios.post(url, body).then(responseBody),
    delete: (url: string) => axios.delete(url).then(responseBody),
    postForm: (url: string, data: FormData) => axios.post(url, data, {
        headers: {'Content-Type': 'multipart/form-data'}
    }).then(responseBody),
    putForm: (url: string, data: any) => axios.put(url, data, {
        headers: {'Content-Type': 'multipart/form-data'}
    }).then(responseBody)
}

function createFormData(item: any){
    let formData =new FormData();
    for(const key in item){
        formData.append(key, item[key])
    }
    return formData;
}

const Catalog = {
    list: () => request.get('api/Products'),
    details: (id: number) => request.get('api/Products/'+id)
}

const Basket = {
    get: () => request.get('Basket'),
    addItem: (productId: number, quantity = 1) => request.post(`Basket?productId=${productId}&quantity=${quantity}`, {}),
    removeItem: (productId: number, quantity = 1) => request.delete(`Basket?productId=${productId}&quantity=${quantity}`)
}

const Account = {
    login: (values: any) => request.post('login', values),
    currentuser: () => request.get('currentUser')
}

const Profile = {
    details: (studCode: number) => request.get('/api/StudentProfile?studentCode='+studCode),
    updateProfile: (values: any) => request.putForm('api/StudentProfile', createFormData(values))
}

const Frm002 = {
    updateFrm002: (values: any) => request.postForm('api/Form002', createFormData(values)),
    updateFamilybackground: (values: any) => request.postForm('api/FamilyBackground', createFormData(values))
}

const Frm001 = {
    getList: () => request.get('api/FRM001'),
    createFrm001: (values: any) => request.postForm('api/FRM001', createFormData(values))
}

const InterviewForm = {
    list: () => request.get('/api/InterviewForm'),
    createForm: (values: any) => request.postForm('api/InterviewForm', createFormData(values))  
}

const agent = {
    Catalog,
    Basket,
    Account,
    InterviewForm,
    Profile,
    Frm002,
    Frm001
}

export default agent;