import React from 'react';
import { Box, Button, Grid, Modal, Typography } from '@mui/material';
import styled from 'styled-components';
import SignatureCanvas from 'react-signature-canvas';
import moment from 'moment';
import LoadingButton from '@mui/lab/LoadingButton';
import { apiURL } from 'src/app/api/agent';

const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    Height: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
};

interface Props {
    f: any
}

export default function Form001FormReport({ f }: Props) {
    const [base64image, setBase64image] = React.useState('');

    React.useEffect(() => {
        if (f) {
            imageUrlToBase64(`${apiURL}/api/images/${f.guardianSignatureString?.replace("Uploads\\", "")}`)
                .then((b64) => setBase64image(b64))
        }
    }, [f]);

    const imageUrlToBase64 = async (url: string) => {
        const data = await fetch(url);
        const blob = await data.blob();
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = () => {
                const base64data = reader.result;
                resolve(base64data);
            };
            reader.onerror = reject;
        });
    };

    return (
        <div style={{
            backgroundColor: '#fff', fontSize: '11px', fontFamily: 'Inter, sans-serif', maxWidth: '600px',
            width: '100%'
        }}>
            <Grid container sm={12}>

                <Grid item textAlign='left' xs={12} sm='auto'>
                    <Img
                        loading="lazy"
                        src='images/logostpaul_png.png'
                    />
                </Grid>
                <Grid item textAlign='right' xs={12} sm={11}>
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-between"
                        borderBottom={1}
                    >
                        <div style={{ marginTop: '2%', borderRight: 'solid 1px', padding: 2 }}>
                            <span style={{ fontSize: '11px' }}>
                                ST. PAUL COLLEGE OF MAKATI
                            </span>
                            <br />
                            <span style={{ fontSize: '7px', marginRight: '23%' }}>
                                D.M. Rivera St., Poblacion, Makati City
                            </span>
                        </div>
                        <div style={{ marginTop: 2 }}>
                            <span style={{ fontSize: '7px', border: 'solid 1px', padding: 2, marginRight: 12 }}>GUI-FRM-001</span>
                            <br />
                            <span style={{ fontSize: '12px', marginRight: 12 }}>Guidance Center</span>
                        </div>
                    </Grid>
                </Grid>


                <Grid item xs={12} sm={12} textAlign='center'>
                    <Typography sx={{ fontSize: 14 }} align='center'>
                        Confidentiality and Disclosure Agreement
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} textAlign='left' marginTop={2}>
                    <Typography align="left" sx={{ fontSize: 10, textIndent: 20 }}>
                        Counseling is a trusting relationship between the counselor and the student; the counselor will
                        maintain the confidentiality of information shared by the client. Interactions with the members of the
                        Guidance Team, including scheduling of or attendance at appointments, content of sessions, progress in
                        counseling, and records are confidential.
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} textAlign='left' marginTop={2}>
                    <Typography align="left" sx={{ fontSize: 10 }}>
                        The following are EXCEPTIONS TO CONFIDENTIALITY:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} textAlign='left' marginTop={1}>
                    <Typography align="left" sx={{ fontSize: 10, marginLeft: 2 }}>
                        1. If you give us written authorization to provide information about your Guidance Center records /
                        counseling to another person or department;
                        <br />
                        <br />
                        2. Counseling interventions will remain confidential unless disclosure of confidential information will
                        avoid or minimize imminent danger to the health or safety of the student or the health or safety of
                        others. In those instances, appropriate individuals/agencies will be notified;
                        <br />
                        <br />
                        3. Counselors are obligated by law and ethics to disclose certain confidential information on suspected
                        abuse or neglect of children;
                        <br />
                        <br />
                        4. If contemplation or commission of an offense as contained in, but not limited to, the Student
                        Handbook which can result to a discipline case, is revealed;
                        <br />
                        <br />
                        5. The Guidance Center works as a team. The guidance facilitator/counselor may consult with
                        colleagues or with other professionals in the field to determine the appropriate options for
                        intervention to provide the best possible care;
                        <br />
                        <br />
                        6. Results from psychological tests are treated with full discretion and the identities of the participants
                        remain anonymous. Test results may be used for better classroom and case management, as well as
                        research purposes of the institution;
                        <br />
                        <br />
                        7. A court order, issued by a judge, may oblige a Guidance Counselor to release information contained
                        in records and/or require a counselor to testify in a court hearing, related to a complaint, report or
                        investigation; with necessary consultation with SPCM Administrators and the school’s legal counsel;
                        <br />
                        <br />
                        8. If the guidance facilitator/counselor, Guidance Center, and/or SPCM has any other legal right or
                        obligation to report.
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} textAlign='left' marginTop={1}>
                    <Typography align="left" sx={{ fontSize: 10, marginLeft: 4 }}>
                        Parents/Guardians of students below 18 years of age have a legal right to ask for information and to
                        represent their children. However, if the guidance facilitator/counselor believes that sharing the
                        information will be harmful to the student, confidentiality will be maintained to the limits of the law.
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} textAlign='left' marginTop={1} borderBottom={1} paddingBottom={2}>
                    <Typography align="left" sx={{ fontSize: 10, fontStyle: 'italic' }}>
                        THIS FORM WILL BE PART OF THE STUDENT’S CUMULATIVE FOLDER AND WILL BE VALID
                        UNTIL THE STUDENT HAS GRADUATED OR HAS WITHDRAWN HIS/HER ENROLMENT FROM THE
                        SCHOOL.
                    </Typography>
                </Grid>

                <Grid item xs={12} sm={12} textAlign='left' marginTop={1}>
                    <Typography variant='h6' align="left" sx={{ fontSize: 10, textDecoration: 'underline' }}>
                        CONFORME:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} textAlign='left' marginTop={1}>
                    <Typography align="left" sx={{ fontSize: 10, textIndent: 20 }}>
                        My signature below affirms that I have read and fully understood the above statements regarding records
                        and confidentiality; and that I have been given the opportunity to ask questions. I hereby abide by the guidelines
                        of confidentiality regarding the guidance counseling services offered by the school.
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={4} textAlign='left' marginLeft={2}>
                </Grid>
                <Grid item xs={12} sm={4} textAlign='left' marginTop={1}>
                    <Img2
                        loading="lazy"
                        srcSet={base64image}
                    />
                </Grid>
                <Grid item xs={12} sm={11} textAlign='left'>.
                    <label style={{ fontWeight: 'bold' }}>Parent/Guardian’s Signature Over Printed Name:</label>
                    <label style={{ textDecoration: 'underline' }}>
                        Ella Bells XX D. Dragon IV
                    </label>
                </Grid>
                <Grid item xs={12} sm={1} textAlign='right'>
                    <label style={{ fontWeight: 'bold' }}>Date:</label>
                    <label style={{ textDecoration: 'underline' }}>
                        {moment().format('MM/DD/YYYY')}
                    </label>
                </Grid>
                <Grid item xs={12} sm={11} textAlign='left'>
                    <label style={{ fontWeight: 'bold' }}>Student’s Signature Over Printed Name:</label>
                    <label style={{ textDecoration: 'underline' }}>
                        Ella Bells XX D. Dragon IV
                    </label>
                </Grid>
                <Grid item xs={12} sm={1} textAlign='right'>
                    <label style={{ fontWeight: 'bold' }}>Date:</label>
                    <label style={{ textDecoration: 'underline' }}>
                        {moment().format('MM/DD/YYYY')}
                    </label>
                </Grid>
            </Grid>
        </div>
    )
}

const Img = styled.img`
    aspect-ratio: 0.97;
    object-fit: auto;
    object-position: center;
    width: 39px;
    box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
    margin-top: 11px;
  `;


const Img2 = styled.img`
  aspect-ratio: 2.56;
  object-fit: auto;
  object-position: center;
  width: 176px;
  border: 'solid'
`;





