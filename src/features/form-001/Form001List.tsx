import React, { useEffect, useRef, useState } from 'react'
import Form001Form from './Form001-Form'
import { Helmet } from 'react-helmet-async'
import PageTitleWrapper from 'src/components/PageTitleWrapper'
import { Container, Grid, Paper, Typography } from '@mui/material'
import Form001FormReport from './Form001-FormReport';
import { DataGrid, GridColDef, GridEventListener, GridToolbar } from '@mui/x-data-grid'
import agent from 'src/app/api/agent'
import jsPDF from 'jspdf'
import html2PDF from 'jspdf-html2canvas'

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 20 },
  { field: 'studentCode', headerName: 'Student Code', width: 100 },
  { field: 'firstName', headerName: 'First name', width: 130 },
  { field: 'lastName', headerName: 'Last name', width: 130 },
  { field: 'dateSigned', headerName: 'Signing Date', width: 230 },
];

export default function Form001List() {
  const [frm001list, setFrm001list] = useState([]);
  const [selectedForm, setSelectedForm] = useState();
  const reportTemplateRef = useRef(null);

  useEffect(() => {
    agent.Frm001.getList()
      .then(res => setFrm001list(res));
  }, []);

  const handleEvent: GridEventListener<'rowClick'> = (
    params, // GridCallbackDetails
  ) => {
    //setMessage(`Movie "${params.row.title}" clicked`);
    console.log(params.row);
    setSelectedForm(params.row);
    //handleGeneratePdf();
  };

  const handleGeneratePdf = async() => {

    const doc = new jsPDF({
      unit: 'px',
      format: 'legal',

    });
    doc.setFont("Times");

    const pdf = await html2PDF(reportTemplateRef.current!, {
      jsPDF: {
        format: 'letter',
      },
      imageType: 'image/jpeg',
      output: './pdf/generate.pdf'
    });

    const blob = await pdf.output('blob');
    window.open(URL.createObjectURL(blob));
  };


  return (
    <>
      <Helmet>
        <title>Forms - Confidentiality and Disclosure Agreement</title>
      </Helmet>
      <PageTitleWrapper>
        <Grid
          container
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid item>
            <Typography variant="h3" component="h3" gutterBottom>
              LIST OF SUBMITTED GUI-FRM-001
            </Typography>
            <Typography variant="subtitle2">Confidentiality and Disclosure Agreement</Typography>
          </Grid>
        </Grid>
      </PageTitleWrapper>
      <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
          <Grid container item xs={12}>
            <Container component={Paper} elevation={24}>

              <Grid container spacing={2} marginTop={2}>
                <Grid item xs={12} sm={12} marginTop={3}>
                  <DataGrid
                    rows={frm001list}
                    columns={columns}
                    initialState={{
                      pagination: {
                        paginationModel: { page: 0, pageSize: 5 },
                      },
                    }}
                    pageSizeOptions={[5, 10]}
                    onRowClick={handleEvent}
                    slots={{ toolbar: GridToolbar }}
                  /* checkboxSelection */
                  />
                </Grid>
                <Grid item xs={12} sm={12} marginTop={3}>
                  <div>
                    <button className="button" onClick={handleGeneratePdf}>
                      Generate PDF
                    </button>
                    {selectedForm && (
                      <div ref={reportTemplateRef} style={{ maxWidth: '800px', width: '100%', }}>
                        <Form001FormReport f={selectedForm} />
                      </div>
                    )}
                  </div>
                </Grid>
              </Grid >
            </Container>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}
