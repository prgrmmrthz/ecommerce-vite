import React from 'react'
import Form001Form from './Form001-Form'
import { Helmet } from 'react-helmet-async'
import PageTitleWrapper from 'src/components/PageTitleWrapper'
import { Container, Grid, Paper, Typography } from '@mui/material'

export default function Form001() {
  return (
    <>
      <Helmet>
        <title>Forms - Confidentiality and Disclosure Agreement</title>
      </Helmet>
      <PageTitleWrapper>
        <Grid
          container
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid item>
            <Typography variant="h3" component="h3" gutterBottom>
              GUI-FRM-001
            </Typography>
            <Typography variant="subtitle2">Confidentiality and Disclosure Agreement</Typography>
          </Grid>
        </Grid>
      </PageTitleWrapper>
      <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
          <Grid container item xs={12}>
            <Container component={Paper} elevation={24}>
              <Form001Form mode={1} />
            </Container>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}
