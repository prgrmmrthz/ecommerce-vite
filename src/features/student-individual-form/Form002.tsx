import React, { useEffect } from 'react'
import { Grid, Typography, Divider, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Controller, FieldValues, SubmitHandler, useForm } from 'react-hook-form';
import moment from 'moment';
import FBTableTans from './FBTableTans';
import SiblingsTableTans from './SiblingsTableTans';
import EBTableTans from './EBTableTans';
import HealthInformation from './HealthInformation';
import GeneralPersonality from './GeneralPersonality';
import SocialRelationship from './SocialRelationship';
import agent from 'src/app/api/agent';
import { useAppSelector } from 'src/app/store/configureStore';
import { CreateForm002Schema } from 'src/app/models/form002';

export default function Form002() {
    const {
        socialRelationshipState, socialRelationshipStateOthers,
        famBGState, healthInformationState, healthInformationStateOthers,
        generalPersonalityState, generalPersonalityStateOthers,
        siblingsState, educationBGState
    } = useAppSelector(state => state.formField);
    const { user } = useAppSelector(state => state.account);
    const { register, handleSubmit, control, setValue, formState: { isSubmitting, errors } } = useForm();

    const onSubmit: SubmitHandler<any> = async (d: FieldValues) => {
        console.log(d);
        const a: CreateForm002Schema = {
            academicYear: d.academicYear,
            fatherName: famBGState[0].name,
            fatherPlaceofBirth: famBGState[0].birthPlace,
            fatherNationality: famBGState[0].nationality,
            fatherReligion: famBGState[0].religion,
            fatherEducation: famBGState[0].educationalAttainment,
            fatherOccupation: famBGState[0].occupation,
            fatherCompanyNameAddress: famBGState[0].nameAddressOfCompany,
            fatherContactNumber: famBGState[0].contactNumber,
            fatherEmail: famBGState[0].email,
            fatherAge: famBGState[0].age,
            fatherDOB: famBGState[0].birthday,
            motherName: famBGState[1].name,
            motherPlaceofBirth: famBGState[1].birthPlace,
            motherNationality: famBGState[1].nationality,
            motherReligion: famBGState[1].religion,
            motherEducation: famBGState[1].educationalAttainment,
            motherOccupation: famBGState[1].occupation,
            motherCompanyNameAddress: famBGState[1].nameAddressOfCompany,
            motherContactNumber: famBGState[1].contactNumber,
            motherEmail: famBGState[1].email,
            motherAge: famBGState[1].age,
            motherDOB: famBGState[1].birthday,
            guardianName: famBGState[2].name,
            guardianPlaceofBirth: famBGState[2].birthPlace,
            guardianNationality: famBGState[2].nationality,
            guardianReligion: famBGState[2].religion,
            guardianEducation: famBGState[2].educationalAttainment,
            guardianOccupation: famBGState[2].occupation,
            guardianCompanyNameAddress: famBGState[2].nameAddressOfCompany,
            guardianContactNumber: famBGState[2].contactNumber,
            guardianEmail: famBGState[2].email,
            guardianAge: famBGState[2].age,
            guardianDOB: famBGState[2].birthday,
            parentsMaritalStatus: d.parentsmaritalStatus,
            numberOfRelativesAtHome: d.numberOfRelativesAtHome,
            numberOfHelpersAtHome: d.numberOfHelpersAtHome,
            numberOfMaleAtHome: d.numberOfMaleAtHome,
            numberOfFemaleAtHome: d.numberOfFemaleAtHome,
            healthInformationList: healthInformationState,
            healthInformationListOthers: healthInformationStateOthers,
            generalPersonalityMakeupList: generalPersonalityState,
            generalPersonalityMakeupListOthers: generalPersonalityStateOthers,
            socialRelationshipList: socialRelationshipState,
            socialRelationshipListOthers: socialRelationshipStateOthers,
            atHomeOurChildToBe: d.atHomeOurChildToBe,
            thingsMakeAngry: d.thingsMakeAngry,
            talentsHobby: d.talentsHobby,
            strongPoints: d.strongPoints,
            weakness: d.weakness,
            commentsSuggestions: d.commentsSuggestions,
            siblings: siblingsState,
            educationalBackgrounds: educationBGState
        }
        console.log(a);
    }

    useEffect(() => {
        if (user) {
            agent.Profile.details(Number(user.studentCode))
                .then(res => {
                    if (res) {
                        const a: string = res.birthday;
                        setValue('lastName', res.lastName);
                        setValue('firstName', res.firstName);
                        setValue('middleInitial', res.middleInitial);
                        setValue('nickname', res.nickname);
                        setValue('gradeYearLevel', res.gradeYearLevel);
                        setValue('section', res.section);
                        setValue('age', res.age);
                        setValue('birthday', a.substring(0, 10));
                        setValue('placeOfBirth', res.placeOfBirth);
                        setValue('nationality', res.nationality);
                        setValue('religion', res.religion);
                        setValue('email', res.email);
                        setValue('address', res.address);
                        setValue('contactNumber', res.contactNumber);
                        setValue('gender', res.gender);
                    }
                });
        }
    }, [user]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2} marginTop={2}>
                <Grid item xs={12} sm={12} marginTop={3}>
                    <Typography variant='h4' align="left">
                        I. PERSONAL DATA
                    </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        label="Last Name"
                        autoFocus
                        size="small"
                        {...register('lastName', { required: 'Last Name is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        label="First Name"
                        autoFocus
                        size="small"

                        {...register('firstName', { required: 'First Name is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={1}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        label="M.I"
                        autoFocus
                        size="small"

                        {...register('middleInitial', { required: 'M.I is required' })}
                        inputProps={{ maxLength: 2 }}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        label="Nickname"
                        autoFocus
                        size="small"

                        {...register('nickname', { required: 'Nickname is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        required
                        fullWidth
                        label="Grade/Year Level"
                        autoFocus
                        size="small"

                        {...register('gradeYearLevel', { required: 'This is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        required
                        fullWidth
                        label="Section"
                        autoFocus
                        size="small"

                        {...register('section', { required: 'This is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        required
                        fullWidth
                        label="Academic Year"
                        autoFocus
                        size="small"

                        {...register('academicYear', { required: 'This is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <Controller
                        name="gender"
                        control={control}
                        defaultValue=""
                        render={({ field }) => (
                            <TextField
                                {...field}
                                select
                                label="Gender"
                                fullWidth
                                size="small"

                            >
                                <MenuItem value="male">Male</MenuItem>
                                <MenuItem value="female">Female</MenuItem>
                            </TextField>
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={1}>
                    <TextField
                        required
                        fullWidth
                        label="Age"
                        autoFocus
                        size="small"

                        type='number'
                        {...register('age', { required: 'This is required' })}
                        error={!!errors.age}
                        InputLabelProps={{ shrink: true }}
                        onInput={(event: any) => {
                            event.target.value = Math.max(0, parseInt(event.target.value)).toString().slice(0, 2)
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField
                        required
                        fullWidth
                        label="Date Of Birth"
                        autoFocus
                        size="small"

                        defaultValue={moment().format('YYYY-MM-DD')}
                        type='date'
                        {...register('birthday', { required: 'This is required' })}
                        error={!!errors.birthday}
                        helperText={errors?.birthday?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={8}>
                    <TextField
                        required
                        fullWidth
                        label="Place Of Birth"
                        autoFocus
                        size="small"

                        {...register('placeOfBirth', { required: 'This is required' })}
                        error={!!errors.placeOfBirth}
                        helperText={errors?.placeOfBirth?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        required
                        fullWidth
                        label="Nationality"
                        autoFocus
                        size="small"

                        {...register('nationality', { required: 'This is required' })}
                        error={!!errors.nationality}
                        helperText={errors?.nationality?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        required
                        fullWidth
                        label="Religion"
                        autoFocus
                        size="small"

                        {...register('religion', { required: 'This is required' })}
                        error={!!errors.religion}
                        helperText={errors?.religion?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        required
                        fullWidth
                        label="Contact No/s"
                        autoFocus
                        size="small"

                        {...register('contactNumber', { required: 'This is required' })}
                        error={!!errors.contactNumber}
                        helperText={errors?.contactNumber?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        required
                        fullWidth
                        label="Email"
                        autoFocus
                        size="small"

                        type='email'
                        {...register('email', { required: 'This is required' })}
                        error={!!errors.email}
                        helperText={errors?.email?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <TextField
                        required
                        fullWidth
                        label="Address"
                        autoFocus
                        size="small"
                        {...register('address', { required: 'This is required' })}
                        error={!!errors.address}
                        helperText={errors?.address?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                    <br />
                </Grid>
                <Grid item xs={12} sm={12} marginTop={3}>
                    <Typography variant='h4' align="left">
                        II.  FAMILY BACKGROUND
                    </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} sm={8}>
                    <Controller
                        name="parentsMaritalStatus"
                        control={control}
                        defaultValue=""
                        render={({ field }) => (
                            <TextField
                                {...field}
                                select
                                label="Parents are?"
                                fullWidth
                                size="small"
                            >
                                <MenuItem value="Married in the church">Married in the church</MenuItem>
                                <MenuItem value="Married civilly">Married civilly</MenuItem>
                                <MenuItem value="Living together">Living together</MenuItem>
                                <MenuItem value="Separated">Separated</MenuItem>
                                <MenuItem value="Divorced/Annulled">Divorced/Annulled</MenuItem>
                                <MenuItem value="Father remarried">Father remarried</MenuItem>
                                <MenuItem value="Mother remarried">Mother remarried</MenuItem>
                            </TextField>
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <FBTableTans />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography variant='h6' align="left" fontWeight='bold'>
                        Number of persons living at home: Members of the family:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField
                        required
                        fullWidth
                        label="Relative/s"
                        autoFocus
                        size="small"
                        type='number'
                        {...register('NumberOfRelativesAtHome', { required: 'This is required' })}
                        error={!!errors.NumberOfRelativesAtHome}
                        helperText={errors?.NumberOfRelativesAtHome?.message as string}
                        onInput={(event: any) => {
                            event.target.value = Math.max(0, parseInt(event.target.value)).toString().slice(0, 2)
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField
                        required
                        fullWidth
                        label="Helper/s"
                        autoFocus
                        size="small"
                        type='number'
                        {...register('NumberOfHelpersAtHome', { required: 'This is required' })}
                        error={!!errors.NumberOfHelpersAtHome}
                        helperText={errors?.NumberOfHelpersAtHome?.message as string}
                        onInput={(event: any) => {
                            event.target.value = Math.max(0, parseInt(event.target.value)).toString().slice(0, 2)
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField
                        required
                        fullWidth
                        label="Male"
                        autoFocus
                        size="small"
                        type='number'
                        {...register('NumberOfMaleAtHome', { required: 'This is required' })}
                        error={!!errors.NumberOfMaleAtHome}
                        helperText={errors?.NumberOfMaleAtHome?.message as string}
                        onInput={(event: any) => {
                            event.target.value = Math.max(0, parseInt(event.target.value)).toString().slice(0, 2)
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField
                        required
                        fullWidth
                        label="Female"
                        autoFocus
                        size="small"
                        type='number'
                        {...register('NumberOfFemaleAtHome', { required: 'This is required' })}
                        error={!!errors.NumberOfFemaleAtHome}
                        helperText={errors?.NumberOfFemaleAtHome?.message as string}
                        onInput={(event: any) => {
                            event.target.value = Math.max(0, parseInt(event.target.value)).toString().slice(0, 2)
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={12} marginTop={3}>
                    <Typography variant='h6' align="left" fontWeight='bold'>
                        Siblings (starting with the eldest)
                    </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <SiblingsTableTans />
                </Grid>
                <Grid item xs={12} sm={12} marginTop={3}>
                    <Typography variant='h4' align="left">
                        III.  Educational Background
                    </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <EBTableTans />
                </Grid>
                <Grid item xs={12} sm={12} marginTop={3}>
                    <Typography variant='h4' align="left">
                        IV. HEALTH INFORMATION
                    </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography variant='h6' align="left">
                        Check if the child has/had any of the following:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <HealthInformation />
                </Grid>
                <Grid item xs={12} sm={12} marginTop={3}>
                    <Typography variant='h4' align="left">
                        V. GENERAL PERSONALITY MAKE-UP
                    </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography variant='h6' align="left">
                        Check any of the following items which you feel describes your child's general personality make-up
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <GeneralPersonality />
                </Grid>
                <Grid item xs={12} sm={12} marginTop={3}>
                    <Typography variant='h4' align="left">
                        VI. SOCIAL RELATIONSHIP
                    </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography align="left" fontWeight={700}>
                        Please check any item which applies to your child
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <SocialRelationship />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <Typography align="left" fontWeight={645}>
                        At home, we find our child to be:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={10}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('atHomeOurChildToBe', { required: 'This is required' })}
                        error={!!errors.atHomeOurChildToBe}
                        helperText={errors?.atHomeOurChildToBe?.message as string}
                    />
                    <br />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <Typography align="left" fontWeight={645}>
                        Things that make him/her angry:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={10}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('thingsMakeAngry', { required: 'This is required' })}
                        error={!!errors.thingsMakeAngry}
                        helperText={errors?.thingsMakeAngry?.message as string}
                    />
                    <br />
                </Grid>
                <Grid item xs={12} sm={1}>
                    <Typography align="left" fontWeight={645}>
                        Talent(s) & Hobby:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={11}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('talentsHobby', { required: 'This is required' })}
                        error={!!errors.talentsHobby}
                        helperText={errors?.talentsHobby?.message as string}
                    />
                    <br />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography align="left" fontWeight={645}>
                        Strong points that you believe you have:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('strongPoints', { required: 'This is required' })}
                        error={!!errors.strongPoints}
                        helperText={errors?.strongPoints?.message as string}
                    />
                    <br />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography align="left" fontWeight={645}>
                        Weakness / Difficulties that you believe you have:
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('weakness', { required: 'This is required' })}
                        error={!!errors.weakness}
                        helperText={errors?.weakness?.message as string}
                    />
                    <br />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography align="left" fontWeight={800}>
                        COMMENTS/SUGGESTIONS/REQUESTS
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <TextField
                        multiline
                        style={{ width: '100%' }}
                        inputProps={{ maxLength: 400 }}
                        {...register('commentsSuggestions', { required: 'This is required' })}
                    />
                    <br />
                </Grid>
                <Grid item xs={12}>
                    <LoadingButton
                        loading={isSubmitting}
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >
                        Submit
                    </LoadingButton>
                </Grid>
            </Grid>
        </form>
    )
}
