import React, { useState, useEffect, CSSProperties } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {
  Column,
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  useReactTable
} from "@tanstack/react-table";
import AddIcon from '@mui/icons-material/Add';
import { useAppDispatch, useAppSelector } from '../../app/store/configureStore';
import { SiblingSchema } from '../../app/models/famBG';
import { updateSiblingsState, addNewSibling, removeSibling } from './formFieldSlice';
import TableCell from '@mui/material/TableCell';
import { Button, MenuItem, TableFooter, TextField } from '@mui/material';
import { Delete } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';

const getCommonPinningStyles = (column: Column<SiblingSchema>): CSSProperties => {
  const isPinned = column.getIsPinned()
  const isLastLeftPinnedColumn =
    isPinned === 'left' && column.getIsLastColumn('left')
  const isFirstRightPinnedColumn =
    isPinned === 'right' && column.getIsFirstColumn('right')

  return {
    boxShadow: isLastLeftPinnedColumn
      ? '-4px 0 4px -4px gray inset'
      : isFirstRightPinnedColumn
        ? '4px 0 4px -4px gray inset'
        : undefined,
    left: isPinned === 'left' ? `${column.getStart('left')}px` : undefined,
    right: isPinned === 'right' ? `${column.getAfter('right')}px` : undefined,
    opacity: isPinned ? 0.95 : 1,
    position: isPinned ? 'sticky' : 'relative',
    width: column.getSize(),
    zIndex: isPinned ? 1 : 0,
    background: "white"
  }
}

type Option = {
  label: string;
  value: string;
};

const TableCellReactTables = ({ getValue, row, column, table }: any) => {
  const initialValue = getValue();
  const [value, setValue] = useState(initialValue);

  useEffect(() => {
    setValue(initialValue)
  }, [initialValue]);

  const onBlur = () => {
    table.options.meta?.updateData(row.original.id, column.id, value)
  }

  const onSelectChange = (e: any) => {
    setValue(e.target.value);
    table.options.meta?.updateData(row.original.id, column.id, e.target.value);
  };

  switch (column.columnDef.meta?.type) {
    case 'select':
      return (
        <TextField
          select
          fullWidth
          size="small"
          onChange={(e) => onSelectChange(e)}
          value={value}
          style={{ width: column.columnDef.meta?.width || '300px' }}
        >
          {column.columnDef.meta?.options?.map((option: Option) => (
            <MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>
          ))}

        </TextField>
      )
      break;
    default:
      return (
        <TextField
          required
          fullWidth
          value={value}
          onChange={e => setValue(e.target.value)}
          onBlur={onBlur}
          type={column.columnDef.meta?.type || "text"}
          size="small"
          style={{ width: column.columnDef.meta?.width || '300px' }}
        />
      )
  }
}

const columnHelper = createColumnHelper<SiblingSchema>();

const columns = [
  columnHelper.accessor("name", {
    header: "Name",
    cell: TableCellReactTables,
    meta: {
      type: "text",
      width: '270px'
    },
  }),
  columnHelper.accessor("sex", {
    header: "Sex",
    cell: TableCellReactTables,
    meta: {
      type: "select",
      width: '100px',
      options: [
        { value: "Male", label: "Male" },
        { value: "Female", label: "Female" }
      ]
    },
  }),
  columnHelper.accessor("age", {
    header: "Age",
    cell: TableCellReactTables,
    meta: {
      type: "number",
      width: '70px'
    },
  }),
  columnHelper.accessor("civilStatus", {
    header: "Civil Status",
    cell: TableCellReactTables,
    meta: {
      type: "select",
      width: '120px',
      options: [
        { value: "Single", label: "Single" },
        { value: "Married", label: "Married" }
      ]
    },
  }),
  columnHelper.accessor("schoolOrOccupation", {
    header: "School or Occupation",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  }),
  columnHelper.accessor("gradeyearCompanyOrFirm", {
    header: "Gr./Yr./Company or Firm",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  })
];

export default function SiblingsTableTans() {
  const { siblingsState, status } = useAppSelector(state => state.formField);
  const dispatch = useAppDispatch();
  const [data, setData] = useState(() => [...siblingsState]);

  const table = useReactTable({
    data,
    columns,
    defaultColumn: {
      size: 200, //starting column size
      minSize: 50, //enforced during column resizing
      maxSize: 500, //enforced during column resizing
    },
    getCoreRowModel: getCoreRowModel(),
    meta: {
      updateData: (rowIndex: number, columnId: string, value: any) => {
        dispatch(updateSiblingsState({ id: rowIndex, newValue: value, columnRef: columnId }));
      }
    }
  });

  useEffect(() => {
    setData([...siblingsState]);
    console.log('called yung use effect');
    return () => {
      // Perform cleanup here (if needed)
    };
  }, [siblingsState]);

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  const { column } = header;
                  return (
                    <TableCell key={header.id} style={{ ...getCommonPinningStyles(column) }}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                          header.column.columnDef.header,
                          header.getContext()
                        )}
                    </TableCell>
                  )
                })}
                <TableCell>
                  Action
                </TableCell>
              </TableRow>
            ))}
          </TableHead>
          <TableBody>
            {table.getRowModel().rows.map((row) => (
              <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                {row.getVisibleCells().map((cell) => (
                  <TableCell component="th" scope="row" key={cell.id} style={{ ...getCommonPinningStyles(cell.column) }}>
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </TableCell>
                ))}
                <TableCell component="th" scope="row" key={0}>
                  <Button variant="outlined" onClick={() => dispatch(removeSibling({id: Number(row.id)}))}>
                    <Delete />
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TableFooter>
            <TableCell>
              <LoadingButton loading={status === 'loading'} variant="outlined" onClick={() => dispatch(addNewSibling(1))}>
                <AddIcon /> Add New Sibling
              </LoadingButton>
            </TableCell>
          </TableFooter>
        </Table>
      </TableContainer>


      <pre>{JSON.stringify(siblingsState, null, "\t")}</pre>
    </>
  );
};