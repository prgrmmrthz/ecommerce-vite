import React, { useState, useEffect, CSSProperties } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {
  Column,
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  useReactTable
} from "@tanstack/react-table";
import { useAppDispatch, useAppSelector } from '../../app/store/configureStore';
import { FamBG } from '../../app/models/famBG';
import { updateFamBGState } from './formFieldSlice';
import TableCell from '@mui/material/TableCell';
import { Divider, Grid, MenuItem, TextField, Typography } from '@mui/material';
import { Controller, FieldValues, SubmitHandler, useForm } from 'react-hook-form';
import { LoadingButton } from '@mui/lab';
import agent from 'src/app/api/agent';
import Swal from 'sweetalert2';
import { FamilyBackground } from 'src/app/models/form002';

const getCommonPinningStyles = (column: Column<FamBG>): CSSProperties => {
  const isPinned = column.getIsPinned()
  const isLastLeftPinnedColumn =
    isPinned === 'left' && column.getIsLastColumn('left')
  const isFirstRightPinnedColumn =
    isPinned === 'right' && column.getIsFirstColumn('right')

  return {
    boxShadow: isLastLeftPinnedColumn
      ? '-4px 0 4px -4px gray inset'
      : isFirstRightPinnedColumn
        ? '4px 0 4px -4px gray inset'
        : undefined,
    left: isPinned === 'left' ? `${column.getStart('left')}px` : undefined,
    right: isPinned === 'right' ? `${column.getAfter('right')}px` : undefined,
    opacity: isPinned ? 0.95 : 1,
    position: isPinned ? 'sticky' : 'relative',
    width: column.getSize(),
    zIndex: isPinned ? 1 : 0,
    background: "white"
  }
}

const TableCellReactTables = ({ getValue, row, column, table }: any) => {
  const initialValue = getValue();
  const [value, setValue] = useState(initialValue);

  useEffect(() => {
    setValue(initialValue)
  }, [initialValue]);

  const onBlur = () => {
    table.options.meta?.updateData(row.original.id, column.id, value)
  }

  return (
    <TextField
      fullWidth
      value={value}
      onChange={e => setValue(e.target.value)}
      onBlur={onBlur}
      type={column.columnDef.meta?.type || "text"}
      size="small"
      style={{ width: column.columnDef.meta?.width || '300px' }}
    />
  )
}

const columnHelper = createColumnHelper<FamBG>();

const columns = [
  columnHelper.accessor("title", {
    header: "Family"
  }),
  columnHelper.accessor("name", {
    header: "Full Name",
    cell: TableCellReactTables,
    meta: {
      type: "text",
      size: 270
    },
  }),
  columnHelper.accessor("age", {
    header: "Age",
    cell: TableCellReactTables,
    meta: {
      type: "number",
      width: '70px'
    },
  }),
  columnHelper.accessor("birthday", {
    header: "Date Of Birth",
    cell: TableCellReactTables,
    meta: {
      type: "date",
      width: '150px'
    },
  }),
  columnHelper.accessor("birthPlace", {
    header: "Place of Birth",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  }),
  columnHelper.accessor("nationality", {
    header: "Nationality",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  }),
  columnHelper.accessor("religion", {
    header: "Religion",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  }),
  columnHelper.accessor("educationalAttainment", {
    header: "Educational Attainment",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  }),
  columnHelper.accessor("occupation", {
    header: "Occupation",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  }),
  columnHelper.accessor("nameAddressOfCompany", {
    header: "Name/Address of Company",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  }),
  columnHelper.accessor("contactNumber", {
    header: "Contact No/s",
    cell: TableCellReactTables,
    meta: {
      type: "number",
    },
  }),
  columnHelper.accessor("email", {
    header: "Email Address",
    cell: TableCellReactTables,
    meta: {
      type: "text",
    },
  }),
];

export default function FBTableTans() {
  const { famBGState, form002Id } = useAppSelector(state => state.formField);
  const dispatch = useAppDispatch();
  const [data] = useState(() => [...famBGState]);
  const { register, handleSubmit, control, setValue, formState: { isSubmitting, errors } } = useForm();
  const table = useReactTable({
    data,
    columns,
    initialState: { columnPinning: { left: ['title'] } },
    defaultColumn: {
      size: 200, //starting column size
      minSize: 50, //enforced during column resizing
      maxSize: 500, //enforced during column resizing
    },
    getCoreRowModel: getCoreRowModel(),
    meta: {
      updateData: (rowIndex: number, columnId: string, value: any) => {
        dispatch(updateFamBGState({ id: rowIndex, newValue: value, columnRef: columnId }));
      },
    },
  });

  const onSubmit: SubmitHandler<any> = async (d: FieldValues) => {
    console.log(d);
    const a: FamilyBackground = {
      Frm002Id: form002Id,
      fatherName: famBGState[0].name,
      fatherPlaceofBirth: famBGState[0].birthPlace,
      fatherNationality: famBGState[0].nationality,
      fatherReligion: famBGState[0].religion,
      fatherEducation: famBGState[0].educationalAttainment,
      fatherOccupation: famBGState[0].occupation,
      fatherCompanyNameAddress: famBGState[0].nameAddressOfCompany,
      fatherContactNumber: famBGState[0].contactNumber,
      fatherEmail: famBGState[0].email,
      fatherAge: famBGState[0].age,
      fatherDOB: famBGState[0].birthday,
      motherName: famBGState[1].name,
      motherPlaceofBirth: famBGState[1].birthPlace,
      motherNationality: famBGState[1].nationality,
      motherReligion: famBGState[1].religion,
      motherEducation: famBGState[1].educationalAttainment,
      motherOccupation: famBGState[1].occupation,
      motherCompanyNameAddress: famBGState[1].nameAddressOfCompany,
      motherContactNumber: famBGState[1].contactNumber,
      motherEmail: famBGState[1].email,
      motherAge: famBGState[1].age,
      motherDOB: famBGState[1].birthday,
      guardianName: famBGState[2].name,
      guardianPlaceofBirth: famBGState[2].birthPlace,
      guardianNationality: famBGState[2].nationality,
      guardianReligion: famBGState[2].religion,
      guardianEducation: famBGState[2].educationalAttainment,
      guardianOccupation: famBGState[2].occupation,
      guardianCompanyNameAddress: famBGState[2].nameAddressOfCompany,
      guardianContactNumber: famBGState[2].contactNumber,
      guardianEmail: famBGState[2].email,
      guardianAge: famBGState[2].age,
      guardianDOB: famBGState[2].birthday,
      parentsMaritalStatus: d.parentsmaritalStatus
    };
    //console.log(JSON.stringify(a));.
    try {
      const response = await agent.Frm002.updateFamilybackground(a);
      console.log('response', response);
      if (response) {
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: "Successful Updating Personal Data",
          text: 'please proceed to next step (Siblings information)',
          showConfirmButton: false,
          timer: 2500
        });
      }
    } catch (error) {
      console.log('error', JSON.stringify(error));
      Swal.fire({
        position: "top-end",
        icon: "error",
        title: "Error Filing Family background",
        /* text: JSON.stringify(error), */
        showConfirmButton: true
      });
    }//tryCatch
  }

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={2} marginTop={2}>
          <Grid item xs={12} sm={12} marginTop={3}>
            <Typography variant='h4' align="left">
              I. FAMILY BACKGROUND
            </Typography>
            <Divider />
          </Grid>
          <Grid item xs={12} sm={8}>
            <Controller
              name="parentsMaritalStatus"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  select
                  label="Parents are?"
                  fullWidth
                  size="small"
                >
                  <MenuItem value="Married in the church">Married in the church</MenuItem>
                  <MenuItem value="Married civilly">Married civilly</MenuItem>
                  <MenuItem value="Living together">Living together</MenuItem>
                  <MenuItem value="Separated">Separated</MenuItem>
                  <MenuItem value="Divorced/Annulled">Divorced/Annulled</MenuItem>
                  <MenuItem value="Father remarried">Father remarried</MenuItem>
                  <MenuItem value="Mother remarried">Mother remarried</MenuItem>
                </TextField>
              )}
            />
          </Grid>
          <Grid item xs={12} sm={12} marginTop={2}>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  {table.getHeaderGroups().map((headerGroup) => (
                    <TableRow key={headerGroup.id}>
                      {headerGroup.headers.map((header) => {
                        const { column } = header;
                        return (
                          <TableCell key={header.id} style={{ ...getCommonPinningStyles(column) }}>
                            {header.isPlaceholder
                              ? null
                              : flexRender(
                                header.column.columnDef.header,
                                header.getContext()
                              )}
                          </TableCell>
                        )
                      })}
                    </TableRow>
                  ))}
                </TableHead>
                <TableBody>
                  {table.getRowModel().rows.map((row) => (
                    <TableRow
                      key={row.id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      {row.getVisibleCells().map((cell) => (
                        <TableCell component="th" scope="row" key={cell.id} style={{ ...getCommonPinningStyles(cell.column) }}>
                          {flexRender(cell.column.columnDef.cell, cell.getContext())}
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
          <Grid item xs={12}>
            <LoadingButton
              loading={isSubmitting}
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Submit
            </LoadingButton>
          </Grid>
        </Grid>
      </form>
      <pre>{JSON.stringify(famBGState, null, "\t")}</pre>
    </>
  );
};