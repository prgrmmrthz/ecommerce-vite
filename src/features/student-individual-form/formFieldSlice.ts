import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { EducationalBGSchema, FamBG, SiblingSchema } from "../../app/models/famBG";
import { OtherHealthInfoSchema } from "src/app/models/form002";

interface FBState {
    form002Id : number;
    famBGState: FamBG[];
    status: string;
    siblingsState: SiblingSchema[];
    educationBGState: EducationalBGSchema[];
    generalPersonalityState: string[];
    generalPersonalityStateOthers: string;
    socialRelationshipState: string[];
    socialRelationshipStateOthers: string;
    healthInformationState: string[];
    healthInformationStateOthers: OtherHealthInfoSchema;
}

const initialState: FBState = {
    form002Id: 0,
    famBGState: [
        {
            id: 1,
            title: 'FATHER',
            name: '',
            age: null,
            birthday: null,
            birthPlace: '',
            nationality: '',
            religion: '',
            educationalAttainment: '',
            occupation: '',
            nameAddressOfCompany: '',
            contactNumber: null,
            email: '',
        },
        {
            id: 2,
            title: 'MOTHER',
            name: '',
            age: null,
            birthday: null,
            birthPlace: '',
            nationality: '',
            religion: '',
            educationalAttainment: '',
            occupation: '',
            nameAddressOfCompany: '',
            contactNumber: null,
            email: ''
        },
        {
            id: 3,
            title: 'GUARDIAN',
            name: '',
            age: null,
            birthday: null,
            birthPlace: '',
            nationality: '',
            religion: '',
            educationalAttainment: '',
            occupation: '',
            nameAddressOfCompany: '',
            contactNumber: null,
            email: ''
        }
    ],
    siblingsState: [],
    educationBGState: [],
    generalPersonalityState: [],
    generalPersonalityStateOthers: '',
    socialRelationshipState: [],
    socialRelationshipStateOthers: '',
    healthInformationStateOthers : {
        previousHospitalization: '',
        previousHospitalizationDate: '',
        specifyOperation: '',
        specifyOperationDate: '',
        pastMedicationsGiven: '',
        foodAllergies: '',
        drugsAllergies: '',
        otherHealthMedications:'',
        IndicatedCondition: '',
        schoolMedication: '',
        professionalname: '',
        professionalWhere: '',
        professionalWhen: '',
        professionalDuration: '',
        professionalConcern: '',
    },
    healthInformationState: [],
    status: 'idle'
}

export const formFieldSlice = createSlice({
    name: 'famBG',
    initialState,
    reducers: {
        updateFamBGState: (state, action: PayloadAction<{ id: number; newValue: any; columnRef: string }>) => {
            //console.log('action', action);
            const { id, newValue, columnRef } = action.payload;
            const index = state.famBGState.findIndex(obj => obj.id === id);
            if (index !== -1) {
                state.famBGState = state.famBGState.map(obj => {
                    if (obj.id === id) {
                        return { ...obj, [columnRef]: newValue }; // Update the value
                    }
                    return obj;
                });

            }
        },
        updateSiblingsState: (state, action: PayloadAction<{ id: number; newValue: any; columnRef: string }>) => {
            const { id, newValue, columnRef } = action.payload;
            const index = state.siblingsState.findIndex(obj => obj.id === id);
            if (index !== -1) {
                state.siblingsState = state.siblingsState.map((obj) => {
                    if (obj.id === id) {
                        return { ...obj, [columnRef]: newValue }; // Update the value
                    }
                    return obj;
                });

            }
        },
        addNewSibling: (state) => {
            state.status = 'loading';
            const lastIdinserted = Math.max(...state.siblingsState.map(o => o.id));
            //console.log('lastIdinserted', lastIdinserted);
            const a: SiblingSchema = {
                id: !state.siblingsState.length ? 1 : lastIdinserted + 1,
                name: '',
                sex: '',
                age: null,
                civilStatus: '',
                schoolOrOccupation: '',
                gradeyearCompanyOrFirm: ''
            }
            state.siblingsState.push(a);
            state.status = 'idle';
        },
        removeSibling: (state, action: PayloadAction<{ id: number }>) => {
            const { id } = action.payload;
            if (id !== -1) {
                state.siblingsState.splice(id, 1);
            }
        },
        updateEBState: (state, action: PayloadAction<{ id: number; newValue: any; columnRef: string }>) => {
            const { id, newValue, columnRef } = action.payload;
            const index = state.educationBGState.findIndex(obj => obj.id === id);
            if (index !== -1) {
                state.educationBGState = state.educationBGState.map(obj => {
                    if (obj.id === id) {
                        return { ...obj, [columnRef]: newValue }; // Update the value
                    }
                    return obj;
                });

            }
        },
        addNewEB: (state) => {
            state.status = 'loading';
            const lastIdinserted = Math.max(...state.educationBGState.map(o => o.id));
            console.log('lastIdinserted', lastIdinserted);
            const a: EducationalBGSchema = {
                id: !state.educationBGState.length ? 1 : lastIdinserted + 1,
                level: '',
                school: '',
                address: '',
                yearsAttended: '',
                honors: ''
            }
            state.educationBGState.push(a);
            state.status = 'idle';
        },
        removeEB: (state, action: PayloadAction<{ id: number }>) => {
            const { id } = action.payload;
            if (id !== -1) {
                state.educationBGState.splice(id, 1);
            }
        },
        updateGPState: (state, action: PayloadAction<{ value: string }>) => {
            const { value } = action.payload;
            const currentIndex = state.generalPersonalityState.indexOf(value);

            if (currentIndex === -1) {
                state.generalPersonalityState.push(value);
            } else {
                state.generalPersonalityState.splice(currentIndex, 1);
            }
        },
        updateGPStateOtherValue: (state, action: PayloadAction<{ value: string }>) => {
            const { value } = action.payload;
            state.generalPersonalityStateOthers = value;
        },
        updateSocialRelationshipState: (state, action: PayloadAction<{ value: string }>) => {
            const { value } = action.payload;
            const currentIndex = state.socialRelationshipState.indexOf(value);

            if (currentIndex === -1) {
                state.socialRelationshipState.push(value);
            } else {
                state.socialRelationshipState.splice(currentIndex, 1);
            }
        },
        updateSocialRelationshipStateOtherValue: (state, action: PayloadAction<{ value: string }>) => {
            const { value } = action.payload;
            state.socialRelationshipStateOthers = value;
        },
        updateHealthinfoState: (state, action: PayloadAction<{ value: string }>) => {
            const { value } = action.payload;
            const currentIndex = state.healthInformationState.indexOf(value);

            if (currentIndex === -1) {
                state.healthInformationState.push(value);
            } else {
                state.healthInformationState.splice(currentIndex, 1);
            }
        },
        updateHealthinfoOthersState: (state, action) => {
            // Use the spread operator to create a copy of the state
            state.healthInformationStateOthers = {
                ...state.healthInformationStateOthers,
                ...action.payload
            };
        },
        updateFrm002Id: (state, action: PayloadAction<{ id: number }>) => {
            state.form002Id = action.payload.id;
        },
    }
})

export const { 
    updateFamBGState, updateSiblingsState, addNewSibling, removeSibling,
     updateEBState, addNewEB, removeEB, updateGPState, updateGPStateOtherValue,
     updateSocialRelationshipState, updateSocialRelationshipStateOtherValue,
     updateHealthinfoState, updateHealthinfoOthersState, updateFrm002Id
     } = formFieldSlice.actions;