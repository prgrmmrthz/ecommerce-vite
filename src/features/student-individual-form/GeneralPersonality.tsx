import React, { useState } from 'react';
import { Checkbox, FormControlLabel, FormGroup, Grid, TextField } from '@mui/material';
import { useAppDispatch, useAppSelector } from '../../app/store/configureStore';
import { updateGPState, updateGPStateOtherValue } from './formFieldSlice';

interface CheckboxItem {
    key: string;
    label: string;
}

export default function GeneralPersonality() {
    const { generalPersonalityState, generalPersonalityStateOthers } = useAppSelector(state => state.formField);
    const dispatch = useAppDispatch();

    const handleToggle = (value: string) => {
        dispatch(updateGPState({ value }));
    };

    const checkboxes: CheckboxItem[] = [
        { key: 'calm', label: 'calm' },
        { key: 'cheerful', label: 'cheerful' },
        { key: 'conscientious', label: 'conscientious' },
        { key: 'depressed mood', label: 'depressed mood' },
        { key: 'easily bored', label: 'easily bored' },
        { key: 'easily exhausted', label: 'easily exhausted' },
        { key: 'feels inferior', label: 'feels inferior' },
        { key: 'friendly', label: 'friendly' },
        { key: 'lacks motivation', label: 'lacks motivation' },
        { key: 'lazy', label: 'lazy' },
        { key: 'lovable', label: 'lovable' },
        { key: 'moody', label: 'moody' },
        { key: 'pessimistic', label: 'pessimistic' },
        { key: 'poor health', label: 'poor health' },
        { key: 'quick-tempered', label: 'quick-tempered' },
        { key: 'quiet', label: 'quiet' },
        { key: 'reserved', label: 'reserved' },
        { key: 'sarcastic', label: 'sarcastic' },
        { key: 'self-confident', label: 'self-confident' },
        { key: 'sensitive', label: 'sensitive' },
        { key: 'shy', label: 'shy' },
        { key: 'stubborn', label: 'stubborn' },
        { key: 'submissive', label: 'submissive' },
        { key: 'suspicious', label: 'suspicious' },
        { key: 'tactful', label: 'tactful' },
        { key: 'talented', label: 'talented' },
        { key: 'talkative', label: 'talkative' },
        { key: 'thoughtful', label: 'thoughtful' },
        { key: 'unhappy', label: 'unhappy' },
        { key: 'withdrawn', label: 'withdrawn' },
        // Add more checkboxes as needed
    ];

    return (
        <FormGroup>
            <Grid container>
                {checkboxes.map((c) => (
                    <Grid item xs={3} key={c.key}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={generalPersonalityState.includes(c.key)}
                                    onChange={() => handleToggle(c.key)}
                                />
                            }
                            label={c.label}
                        />
                    </Grid>
                ))}
                <Grid item key={0}>
                    <FormControlLabel
                        control={

                            <>
                                <Checkbox
                                    checked={generalPersonalityState.includes('others')}
                                    onChange={() => handleToggle('others')}
                                />
                            </>
                        }
                        label='Others'
                    />

                </Grid>
                <Grid item xs={8} key={1}>
                    {generalPersonalityState.includes('others') && (<TextField
                        fullWidth
                        size='small'
                        label="(Please Specify)"
                        onBlur={(e) => dispatch(updateGPStateOtherValue({value: e.target.value}))}
                    />)}
                </Grid>
            </Grid>
            <pre>{JSON.stringify(generalPersonalityState, null, "\t")}</pre>
            <pre>{generalPersonalityStateOthers}</pre>
        </FormGroup>
    );
};
