import React, { useState } from 'react';
import { Checkbox, FormControlLabel, FormGroup, Grid, TextField, Typography } from '@mui/material';
import { useAppDispatch, useAppSelector } from '../../app/store/configureStore';
import { updateHealthinfoOthersState, updateHealthinfoState } from './formFieldSlice';
import { useForm, useWatch } from 'react-hook-form';

interface CheckboxItem {
    label: string;
}

const checkboxes: CheckboxItem[] = [
    { label: 'frequesnt URI & tonsilitis' },
    { label: 'heart disease' },
    { label: 'mild epilepsy / seizure febrile' },
    { label: 'frequent headache' },
    { label: 'typhoid fever' },
    { label: 'mumps' },
    { label: 'pneumonia' },
    { label: 'measles' },
    { label: 'accident' },
    { label: 'nose bleeding' },
    { label: 'asthma' },
    { label: 'whooping cough' },
    { label: 'kidney disease' },
    { label: 'diziness' },
    { label: 'polio' },
    { label: 'chicken pox' },
    { label: 'vomitting' },
];

const checkboxes2: CheckboxItem[] = [
    { label: 'Counselor' },
    { label: 'Psychologist' },
    { label: 'Psychiatrist' },
    { label: 'Developmental Pediatrician' },
];

const checkboxes3: CheckboxItem[] = [
    { label: 'ADHD' },
    { label: 'Depression' },
    { label: 'ADD' },
    { label: 'Anxiety' },
    { label: 'Autism / Asperger' },
    { label: 'Bipolar' },
    { label: 'learning Disoreder(e.g. dyslexia)' }
];

export default function SocialRelationship() {
    const { healthInformationState, healthInformationStateOthers } = useAppSelector(state => state.formField);
    const dispatch = useAppDispatch();

    const handleToggle = (value: string) => {
        dispatch(updateHealthinfoState({ value }));
    };

    return (
        <FormGroup>
            <Grid container marginBottom={1}>
                {checkboxes.map((c) => (
                    <Grid item xs={3} key={c.label}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={healthInformationState.includes(c.label)}
                                    onChange={() => handleToggle(c.label)}
                                />
                            }
                            label={c.label}
                        />
                    </Grid>
                ))}
            </Grid>
            <Grid container marginBottom={1}>
                <Grid item xs={3} key={0}>
                    <FormControlLabel
                        control={
                            <>
                                <Checkbox
                                    checked={healthInformationState.includes('prevHospi')}
                                    onChange={() => handleToggle('prevHospi')}
                                />
                            </>
                        }
                        label='previous hospitalization illness'
                    />
                </Grid>
                {healthInformationState.includes('prevHospi') && (
                    <>
                        <Grid item xs={6} marginRight={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Previous hospitalization illness"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ previousHospitalization: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <TextField
                                fullWidth
                                size='small'
                                type='date'
                                onChange={(e) => dispatch(updateHealthinfoOthersState({ previousHospitalizationDate: e.target.value }))}
                            />
                        </Grid>
                    </>
                )}
            </Grid>
            <Grid container marginBottom={1}>
                <Grid item xs={3} key={0}>
                    <FormControlLabel
                        control={
                            <>
                                <Checkbox
                                    checked={healthInformationState.includes('hasOperation')}
                                    onChange={() => handleToggle('hasOperation')}
                                />
                            </>
                        }
                        label='Operation (specify)'
                    />
                </Grid>
                {healthInformationState.includes('hasOperation') && (
                    <>
                        <Grid item xs={6} marginRight={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Specify Operation"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ specifyOperation: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={2} key={2}>
                            <TextField
                                fullWidth
                                size='small'
                                type='date'
                                onChange={(e) => dispatch(updateHealthinfoOthersState({ specifyOperationDate: e.target.value }))}
                            />
                        </Grid>
                    </>
                )}
            </Grid>
            <Grid container marginBottom={1}>
                <Grid item xs={3} key={0}>
                    <FormControlLabel
                        control={
                            <>
                                <Checkbox
                                    checked={healthInformationState.includes('hasAllergies')}
                                    onChange={() => handleToggle('hasAllergies')}
                                />
                            </>
                        }
                        label='allergies (specify)'
                    />
                </Grid>
                {healthInformationState.includes('hasAllergies') && (
                    <>
                        <Grid item xs={4} marginRight={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Food Allergies"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ foodAllergies: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Drugs Allergies"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ drugsAllergies: e.target.value }))}
                            />
                        </Grid>
                    </>
                )}
            </Grid>
            <Grid container marginBottom={1}>
                <Grid item xs={1}>
                    <FormControlLabel
                        control={
                            <>
                                <Checkbox
                                    checked={healthInformationState.includes('others')}
                                    onChange={() => handleToggle('others')}
                                />
                            </>
                        }
                        label='others'
                    />
                </Grid>
                {healthInformationState.includes('others') && (
                    <>
                        <Grid item xs={5} marginRight={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Please specify other medications"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ otherHealthMedications: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={5}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Past medications"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ pastMedicationsGiven: e.target.value }))}
                            />
                        </Grid>
                    </>
                )}
            </Grid>

            <Grid container marginBottom={2}>
                <Grid item xs={4} key={0} marginBottom={1}>
                    <FormControlLabel
                        control={
                            <>
                                <Checkbox
                                    checked={healthInformationState.includes('Presently under the care of medical doctor')}
                                    onChange={() => handleToggle('Presently under the care of medical doctor')}
                                />
                            </>
                        }
                        label='Presently under the care of medical doctor'
                    />
                </Grid>
                {healthInformationState.includes('Presently under the care of medical doctor') && (
                    <>
                        <Grid item xs={8} marginBottom={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Indicate condition and medications"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ IndicatedCondition: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                size='small'
                                label="If Medication to be given in the school, please present prescription of attending physician"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ schoolMedication: e.target.value }))}
                            />
                        </Grid>
                    </>
                )}
            </Grid>
            <Grid container marginBottom={1}>
                <Grid item xs={12}>
                    <Typography variant='h6' align="left">
                        Have you consulted any of the following professional/s before?
                    </Typography>
                </Grid>
                {checkboxes2.map((c) => (
                    <Grid item xs={3} key={c.label}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={healthInformationState.includes(c.label)}
                                    onChange={() => handleToggle(c.label)}
                                />
                            }
                            label={c.label}
                        />
                    </Grid>
                ))}
                {healthInformationState.includes('Counselor') && (
                    <>
                        <Grid item xs={1} marginBottom={1}>
                            <Typography align="left">
                                If yes:
                            </Typography>
                        </Grid>
                        <Grid item xs={5} marginRight={1} marginBottom={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Name:"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ professionalname: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={5} marginBottom={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="When:"
                                type='date'
                                onChange={(e) => dispatch(updateHealthinfoOthersState({ professionalWhen: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={1} marginBottom={1}>
                            
                        </Grid>
                        <Grid item xs={6} marginRight={1} marginBottom={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Where:"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ professionalWhere: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Duration:"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ professionalDuration: e.target.value }))}
                            />
                        </Grid>
                        <Grid item xs={11} marginLeft={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Concern:"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ professionalConcern: e.target.value }))}
                            />
                        </Grid>
                    </>
                )}
            </Grid>
            <Grid container marginBottom={1}>
                <Grid item xs={12}>
                    <Typography variant='h6' align="left" fontWeight={880}>
                        Diagnosis: Check all that applies
                    </Typography>
                </Grid>
                {checkboxes3.map((c) => (
                    <Grid item xs={3} key={c.label}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={healthInformationState.includes(c.label)}
                                    onChange={() => handleToggle(c.label)}
                                />
                            }
                            label={c.label}
                        />
                    </Grid>
                ))}
                <Grid item xs={2} marginRight={11}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={healthInformationState.includes('Other Diagnosis')}
                                    onChange={() => handleToggle('Other Diagnosis')}
                                />
                            }
                            label='Other Diagnosis'
                        />
                    </Grid>
                {healthInformationState.includes('Other Diagnosis') && (
                    <>
                        <Grid item xs={8} marginBottom={1}>
                            <TextField
                                fullWidth
                                size='small'
                                label="Please specify other diagnosis here"
                                onBlur={(e) => dispatch(updateHealthinfoOthersState({ IndicatedCondition: e.target.value }))}
                            />
                        </Grid>
                    </>
                )}
            </Grid>
            <pre>{JSON.stringify(healthInformationState, null, "\t")}</pre>
            <pre>{JSON.stringify(healthInformationStateOthers, null, "\t")}</pre>
        </FormGroup>
    );
};
