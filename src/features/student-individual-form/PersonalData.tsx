import React, { useEffect } from 'react'
import { Grid, Typography, Divider, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Controller, FieldValues, SubmitHandler, useForm } from 'react-hook-form';
import moment from 'moment';
import agent from 'src/app/api/agent';
import { useAppDispatch, useAppSelector } from 'src/app/store/configureStore';
import { PersonalData } from 'src/app/models/form002';
import Swal from 'sweetalert2';
import { updateFrm002Id } from './formFieldSlice';

export default function PersonalDataForm() {
    const dispatch = useAppDispatch();
    const { user } = useAppSelector(state => state.account);
    const { register, handleSubmit, control, setValue, formState: { isSubmitting, errors } } = useForm();

    const onSubmit: SubmitHandler<any> = async (d: FieldValues) => {
        console.log(d);
    if (user) {
      const a: PersonalData = {
        academicYear: d.academicYear,
        lastName: d.lastName,
        firstName: d.firstName,
        middleInitial: d.middleInitial,
        nickname: d.nickname,
        gradeYearLevel: d.gradeYearLevel,
        section: d.section,
        age: d.age,
        birthday: d.birthday,
        placeOfBirth: d.placeOfBirth,
        nationality: d.nationality,
        religion: d.religion,
        email: d.email,
        address: d.address,
        contactNumber: d.contactNumber,
        studentCode: user?.studentCode,
        gender: d.gender
      }//a

      try {
        console.log('data pass as a', a);
        const response = await agent.Frm002.updateFrm002(a);
        console.log('response', response);
        if (response) {
            dispatch(updateFrm002Id({id: response}));
          Swal.fire({
            position: "top-end",
            icon: "success",
            title: "Successful Updating Personal Data",
            text: 'please proceed to next step (Family Background)',
            showConfirmButton: false,
            timer: 2500
          });
        }
      } catch (error) {
        console.log('error', JSON.stringify(error));
        Swal.fire({
          position: "top-end",
          icon: "error",
          title: "Error Filing Personal Data",
          /* text: JSON.stringify(error), */
          showConfirmButton: true
        });
      }//tryCatch

    }
    }

    useEffect(() => {
        if (user) {
            agent.Profile.details(Number(user.studentCode))
                .then(res => {
                    if (res) {
                        const a: string = res.birthday;
                        setValue('lastName', res.lastName);
                        setValue('firstName', res.firstName);
                        setValue('middleInitial', res.middleInitial);
                        setValue('nickname', res.nickname);
                        setValue('gradeYearLevel', res.gradeYearLevel);
                        setValue('section', res.section);
                        setValue('age', res.age);
                        setValue('birthday', a.substring(0, 10));
                        setValue('placeOfBirth', res.placeOfBirth);
                        setValue('nationality', res.nationality);
                        setValue('religion', res.religion);
                        setValue('email', res.email);
                        setValue('address', res.address);
                        setValue('contactNumber', res.contactNumber);
                        setValue('gender', res.gender);
                    }
                });
        }
    }, [user]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2} marginTop={2}>
                <Grid item xs={12} sm={12} marginTop={3}>
                    <Typography variant='h4' align="left">
                        I. PERSONAL DATA
                    </Typography>
                    <Divider />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        label="Last Name"
                        autoFocus
                        size="small"
                        {...register('lastName', { required: 'Last Name is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        label="First Name"
                        autoFocus
                        size="small"

                        {...register('firstName', { required: 'First Name is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={1}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        label="M.I"
                        autoFocus
                        size="small"

                        {...register('middleInitial', { required: 'M.I is required' })}
                        inputProps={{ maxLength: 2 }}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        label="Nickname"
                        autoFocus
                        size="small"

                        {...register('nickname', { required: 'Nickname is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        required
                        fullWidth
                        label="Grade/Year Level"
                        autoFocus
                        size="small"

                        {...register('gradeYearLevel', { required: 'This is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        required
                        fullWidth
                        label="Section"
                        autoFocus
                        size="small"

                        {...register('section', { required: 'This is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <TextField
                        required
                        fullWidth
                        label="Academic Year"
                        autoFocus
                        size="small"

                        {...register('academicYear', { required: 'This is required' })}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <Controller
                        name="gender"
                        control={control}
                        defaultValue=""
                        render={({ field }) => (
                            <TextField
                                {...field}
                                select
                                label="Gender"
                                fullWidth
                                size="small"

                            >
                                <MenuItem value="male">Male</MenuItem>
                                <MenuItem value="female">Female</MenuItem>
                            </TextField>
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={1}>
                    <TextField
                        required
                        fullWidth
                        label="Age"
                        autoFocus
                        size="small"

                        type='number'
                        {...register('age', { required: 'This is required' })}
                        error={!!errors.age}
                        InputLabelProps={{ shrink: true }}
                        onInput={(event: any) => {
                            event.target.value = Math.max(0, parseInt(event.target.value)).toString().slice(0, 2)
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField
                        required
                        fullWidth
                        label="Date Of Birth"
                        autoFocus
                        size="small"

                        defaultValue={moment().format('YYYY-MM-DD')}
                        type='date'
                        {...register('birthday', { required: 'This is required' })}
                        error={!!errors.birthday}
                        helperText={errors?.birthday?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={8}>
                    <TextField
                        required
                        fullWidth
                        label="Place Of Birth"
                        autoFocus
                        size="small"

                        {...register('placeOfBirth', { required: 'This is required' })}
                        error={!!errors.placeOfBirth}
                        helperText={errors?.placeOfBirth?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        required
                        fullWidth
                        label="Nationality"
                        autoFocus
                        size="small"

                        {...register('nationality', { required: 'This is required' })}
                        error={!!errors.nationality}
                        helperText={errors?.nationality?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        required
                        fullWidth
                        label="Religion"
                        autoFocus
                        size="small"

                        {...register('religion', { required: 'This is required' })}
                        error={!!errors.religion}
                        helperText={errors?.religion?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        required
                        fullWidth
                        label="Contact No/s"
                        autoFocus
                        size="small"

                        {...register('contactNumber', { required: 'This is required' })}
                        error={!!errors.contactNumber}
                        helperText={errors?.contactNumber?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <TextField
                        required
                        fullWidth
                        label="Email"
                        autoFocus
                        size="small"

                        type='email'
                        {...register('email', { required: 'This is required' })}
                        error={!!errors.email}
                        helperText={errors?.email?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
                <Grid item xs={12} sm={12}>
                    <TextField
                        required
                        fullWidth
                        label="Address"
                        autoFocus
                        size="small"
                        {...register('address', { required: 'This is required' })}
                        error={!!errors.address}
                        helperText={errors?.address?.message as string}
                        InputLabelProps={{ shrink: true }}
                    />
                    <br />
                </Grid>
                <Grid item xs={12}>
                    <LoadingButton
                        loading={isSubmitting}
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >
                        Submit
                    </LoadingButton>
                </Grid>
            </Grid>
        </form>
    )
}
