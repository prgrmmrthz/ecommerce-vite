
import React from 'react';

import { Helmet } from 'react-helmet-async';
import PageTitleWrapper from '../../components/PageTitleWrapper';
import PageTitle from '../../components/PageTitle';
import { Container, Grid, Paper } from '@mui/material';
import Form002 from './Form002';
import FormStepper from './FormStepper';

export default function StudentIndividualForm() {
  return (
    <>
      <Helmet>
        <title>Forms - Components</title>
      </Helmet>
      <PageTitleWrapper>
        <PageTitle
          heading="GUI-FRM-002"
          subHeading="Student Individual Inventory Form"
        />
      </PageTitleWrapper>
      <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
          <Grid container item xs={12}>
            <Container component={Paper} elevation={10}>
              <FormStepper />
            </Container>
          </Grid>
        </Grid>
      </Container>
    </>

  )
}
