import React from 'react';
import { Checkbox, FormControlLabel, FormGroup, Grid, TextField } from '@mui/material';
import { useAppDispatch, useAppSelector } from '../../app/store/configureStore';
import { updateSocialRelationshipState, updateSocialRelationshipStateOtherValue } from './formFieldSlice';

interface CheckboxItem {
    label: string;
}

export default function SocialRelationship() {
    const { socialRelationshipState, socialRelationshipStateOthers } = useAppSelector(state => state.formField);
    const dispatch = useAppDispatch();

    const handleToggle = (value: string) => {
        dispatch(updateSocialRelationshipState({ value }));
    };

    const checkboxes: CheckboxItem[] = [
        {label: 'discusses problems with father'},
        {label: 'discusses problems with mother'},
        {label: 'enjoys company of brother and sister'},
        {label: 'prefers to be left alone'},
        {label: 'generous with his/her siblings'},
        {label: 'friendly with household help'},
        {label: 'enjoys family outing and affairs'}
    ];

    return (
        <FormGroup>
            <Grid container>
                {checkboxes.map((c) => (
                    <Grid item xs={4} key={c.label}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={socialRelationshipState.includes(c.label)}
                                    onChange={() => handleToggle(c.label)}
                                />
                            }
                            label={c.label}
                        />
                    </Grid>
                ))}
                <Grid item key={0}>
                    <FormControlLabel
                        control={

                            <>
                                <Checkbox
                                    checked={socialRelationshipState.includes('Others')}
                                    onChange={() => handleToggle('Others')}
                                />
                            </>
                        }
                        label='Others'
                    />

                </Grid>
                <Grid item xs={6} key={1}>
                    {socialRelationshipState.includes('Others') && (<TextField
                        fullWidth
                        size='small'
                        label="(Please Specify)"
                        onBlur={(e) => dispatch(updateSocialRelationshipStateOtherValue({value: e.target.value}))}
                    />)}
                </Grid>
            </Grid>
            <pre>{JSON.stringify(socialRelationshipState, null, "\t")}</pre>
            <pre>{socialRelationshipStateOthers}</pre>
        </FormGroup>
    );
};
