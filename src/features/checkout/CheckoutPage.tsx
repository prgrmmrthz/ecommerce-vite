import React from 'react'

export default function CheckoutPage() {
    return (
        <div>
            only logged in users should be able to see this
        </div>
    )
}
