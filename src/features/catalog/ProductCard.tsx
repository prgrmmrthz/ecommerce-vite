import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  CardHeader,
} from "@mui/material";
import React from "react";
import { Product } from "../../app/models/product";
import { Link } from "react-router-dom";
import { LoadingButton } from "@mui/lab";
import { currencyFormat } from "../../app/util/util";
import { useAppDispatch, useAppSelector } from "../../app/store/configureStore";
import { addBasketItemAsync } from "../basket/basketSlice";

interface Props {
  product: Product;
}

export default function ProductCard({ product }: Props) {
  const {basket, status} = useAppSelector(state => state.basket);
  const dispatch = useAppDispatch();

  const findItemValueInTheCart = (productID: number) => {
    const itemIndex = basket?.items.findIndex(i => i.productId === productID);
    if (itemIndex === -1 || itemIndex === undefined) return;
    return basket?.items[itemIndex].quantity;
  }

  return (
    <Card>
      <CardMedia
        sx={{ height: 140, backgroundSize: 'contain'}}
        image={product.pictureUrl}
        title={product.name}
      />
      <CardHeader
        avatar={<Avatar>{product.name.charAt(0).toUpperCase()}</Avatar>}
        title={product.name}
      ></CardHeader>
      <CardContent>
        <Typography gutterBottom color="secondary" variant="h5">
          {currencyFormat(product.price)}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {product.classification}
        </Typography>
      </CardContent>
      <CardActions>
        <LoadingButton
          loading={status === 'pendingAddItem' + product.id}
          onClick={() => dispatch(addBasketItemAsync({productId: product.id}))}
          size="small"
        >
          Add To Cart [{(!findItemValueInTheCart(product.id) ? 0 : findItemValueInTheCart(product.id))}]
        </LoadingButton>
        <Button component={Link} to={`/catalog/${product.id}`} size="small">
          View [{product.id}]
        </Button>
      </CardActions>
    </Card>
  );
}
