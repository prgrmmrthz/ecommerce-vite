import React, { useEffect } from "react";
import ProductsList from "./ProductsList";
import LoadingComponent from "../../app/layout/LoadingComponent";
import { useAppDispatch, useAppSelector } from "../../app/store/configureStore";
import { fetchProductsListAsync, productSelectors } from "./catalogSlice";

export default function Catalog() {
  const products = useAppSelector(productSelectors.selectAll);
  const {productsLoaded, status} = useAppSelector(state => state.catalog);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!productsLoaded) dispatch(fetchProductsListAsync());
  }, [dispatch, productsLoaded]);

  if(status.includes('pending')) return <LoadingComponent message="Loading products..."/>

  return (
    <>
      <ProductsList products={products} />
    </>
  );
}
