import * as React from "react";
import styled from "styled-components";
import { InterviewFormSchema } from "../../app/models/interviewFormSchema";
import { Grid, Typography } from "@mui/material";
import moment from "moment";
import { apiURL } from "../../app/api/agent";

interface Props {
  f: InterviewFormSchema
}

export default function MyDocument({ f }: Props) {
  return (
    <div className="XXX">
      <Grid container sm={12}>

        <Div>
          <Div2>
            <Div3 style={{ marginTop: 3 }}>
              <Img
                loading="lazy"
                src='images/logostpaul_png.png'
              />
              <Div4>
                <Div5>
                  <Div6>
                    <span style={{ fontSize: '11px' }}>
                      ST. PAUL COLLEGE OF MAKATI{" "}
                    </span>
                    <br />
                    <span style={{ fontSize: '7px' }}>
                      D.M. Rivera St., Poblacion, Makati City
                    </span>
                  </Div6>
                  <Div7>
                    <Div8>GUI-FRM-003</Div8>
                    <Div9>Guidance Center</Div9>
                  </Div7>
                </Div5>
              </Div4>
            </Div3>
            <Div10>CONFIDENTIAL</Div10>
            <Div11>
              Initial Interview Form
              <br />
              <span style={{ fontSize: '12px', fontWeight: 300 }}>
                School Year 2024-2025
              </span>
            </Div11>
          </Div2>
          <Div12>
            <Div13>
              <Div14>
                <Div15>Name: </Div15>
                <Div16>{f.lastName}, {f.firstName}, {f.middleInitial}</Div16>
              </Div14>
              <Div17>
                <Div18>Nickname: </Div18>
                <Div19>{f.nickname}</Div19>
              </Div17>
              <Div20>
                <Div21>Grade & Section:</Div21>
                <Div22>{f.grade} - {f.section}</Div22>
              </Div20>
            </Div13>
            <Div23>
              <Div24>
                <Div25>Age: </Div25>
                <Div26>{f.age}</Div26>
              </Div24>
              <Div27>
                <Div28>Birthday: </Div28>
                <Div29>{moment(f.birthday).format('MM/DD/YYYY')}</Div29>
                <Div30>Religion:</Div30>
                <Div31>Roman Catholic Cath</Div31>
              </Div27>
              <Div32>
                <Div33>Citizenship:</Div33>
                <Div34>{f.citizenship}</Div34>
              </Div32>
            </Div23>
            <Div35>
              <Div36>
                STATEMENT OF DATA PRIVACY, CONFIDENTIALITY, AND RELEASE OF
                INFORMATION
              </Div36>
              <Div37>
                The Guidance Center adheres to strict data privacy and
                confidentiality standards. Any Information or counseling records
                that you provide are strictly confidential, except in life
                threatening situations, cases of suspected abuse, or when release is
                otherwise required by law.
                <br />
                <br />
                To protect your data privacy and your right to confidentiality, your
                written authorization is required if you want us to provide
                information about your records/counseling to another person or
                department. Information about your counseling will not appear on
                your academic record.
                <br />
                <br />
                In order to provide the best possible care, your guidance
                facilitator/counselor may consult with colleagues or other
                professionals in the field. Rest assured that no identifying
                information will be disclosed.
                <br />
                <br />
                As a minor, your parents and/or guardians have a legal right to ask
                for information and to represent their children. Please write your
                name and sign below to indicate that you have read and understood
                the above statements regarding records, data privacy, and
                confidentiality. If you have any questions, you may ask your
                guidance facilitator/counselor.
              </Div37>
            </Div35>
            <Div38>
              <Div39>
                <Img2
                  loading="lazy"
                  srcSet={`${apiURL}/api/images/${f.fileString?.replace("Uploads\\", "")}`}
                />
                <Div100>{f.firstName} {f.middleInitial} {f.lastName}</Div100>
                <Div40>Student’s Signature over Printed Name</Div40>
              </Div39>
              <Div41>
                <Div42>{moment(f.dateFiled).format('MM/DD/YYYY')}</Div42>
                <Div43>Date</Div43>
              </Div41>
            </Div38>
            <Div44>
              In the event that my guidance facilitator/counselor reasonably
              believes that I am in danger to myself or others, I allow my guidance
              facilitator/counselor to contact the following persons, in addition to
              the school and medical personnel.
            </Div44>
            <Div45>
              <Div46>
                <Div47>
                  <Box>
                    <Box2>
                      <Div49>NAME</Div49>
                      <Div50>{f.firstContactName}</Div50>
                    </Box2>
                  </Box>
                </Div47>
                <Div51>
                  <Div52>RELATIONSHIP</Div52>
                  <Div53>{f.firstContactRelationship}</Div53>
                </Div51>
                <Div54>
                  <Div55>CONTACT #</Div55>
                  <Div56>{f.firstContactContactnumber}</Div56>
                </Div54>
              </Div46>
              <Div146>
                <Div147>
                  <Box100>
                    <Box200>
                      <Div150>{f.secondContactName}</Div150>
                    </Box200>
                  </Box100>
                </Div147>
                <Div151>
                  <Div153>{f.secondContactRelationship}</Div153>
                </Div151>
                <Div154>
                  <Div156>{f.secondContactContactnumber}</Div156>
                </Div154>
              </Div146>
            </Div45>
          </Div12>
        </Div>
      </Grid>

      <div style={{ marginTop: 100, maxWidth: '600px', width: '100%', backgroundColor: '#fff', fontSize: '11px', fontFamily: 'Inter, sans-serif' }}>
        <Grid container sx={{ margin: 2 }}>
          <Grid item xs={12} sm={12} textAlign='right' marginRight={4} marginTop={2}>
            <span style={{ alignItems: 'left' }}>
              GUI-FRM-003
            </span>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Typography align="left" fontWeight={600}>
              CONFIDENTIAL
            </Typography>
          </Grid>
          <Grid item xs={12} sm={12} marginTop={2}>
            <Typography align="left" sx={{ fontSize: 14 }}>
              FAMILY BACKGROUND
            </Typography>
          </Grid>
          <Grid item xs={12} sm={1}>
            Father:
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.isFatherLiving === 'living'} />
            <label>Living</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.isFatherLiving !== 'living'} />
            <label>Deceased</label>
          </Grid>
          <Grid item xs={12} sm={1}>
            Mother:
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.isMotherLiving === 'living'} />
            <label>Living</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.isMotherLiving !== 'living'} />
            <label>Deceased</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            Parent's Marital Status:
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.parentsMaritalStatus === 'married'} />
            <label>Married</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.parentsMaritalStatus === 'livingtogether'} />
            <label>Living Together</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            <input type="checkbox" checked={f.parentsMaritalStatus === 'singleparent'} />
            <label>Single Parent</label>
          </Grid>
          <Grid item xs={12} sm={3}></Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.parentsMaritalStatus === 'seperated'} />
            <label>Seperated</label>
          </Grid>
          <Grid item xs={12} sm={6}>
            <input type="checkbox" checked={f.parentsMaritalStatus === 'other'} />
            <label>others: </label>
            <label style={{ textDecoration: 'underline' }}>{f.otherParentsMaritalStatus || '______'}</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            Living with:
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.livingWith === 'biologicalparent'} />
            <label>Both Biological Parent</label>
          </Grid>
          <Grid item xs={12} sm={6}>
            <input type="checkbox" checked={f.livingWith === 'biologicalmotherandstepfather'} />
            <label>Biological Mother & Step Father</label>
          </Grid>
          <Grid item xs={12} sm={2}>
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.livingWith === 'biologicalfatherandstepmother'} />
            <label>Biological Father & Step Mother</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            <input type="checkbox" checked={f.livingWith === 'motheronly'} />
            <label>Mother Only</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            <input type="checkbox" checked={f.livingWith === 'fatheronly'} />
            <label>Father Only</label>
          </Grid>
          <Grid item xs={12} sm={2}>
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.livingWith === 'stepmotheronly'} />
            <label>Step Mother Only</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            <input type="checkbox" checked={f.livingWith === 'stepfatheronly'} />
            <label>Step Father Only</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.livingWith === 'separated'} />
            <label>Separated</label>
          </Grid>
          <Grid item xs={12} sm={2}>
          </Grid>
          <Grid item xs={12} sm={10}>
            <input type="checkbox" checked={f.livingWith === 'guardian'} />
            <label>Guardian (relationship): </label>
            <label style={{ textDecoration: 'underline' }}>{f.otherLivingWith || '______'}</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            Number of Siblings
          </Grid>
          <Grid item xs={12} sm={2}>
            <label style={{ textDecoration: 'underline' }}>{f.numberOfSiblingsBrother || '___'}</label>
            <label> Brother(s)</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            <label style={{ textDecoration: 'underline' }}>{f.numberOfSiblingsSister || '___'}</label>
            <label> Sister(s)</label>
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.isOnlyChild} />
            <label>none</label>
          </Grid>
          <Grid item xs={12} sm={12} marginTop={2}>
            <Typography align="left" sx={{ fontSize: 14 }}>
              Parent's Occupational Status
            </Typography>
          </Grid>
          <Grid item xs={12} sm={1}>
            Father:
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.fathersOccupationalStatus === 'workingwithinthecountry'} />
            <label>working within the country</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            <input type="checkbox" checked={f.fathersOccupationalStatus === 'selfemployed'} />
            <label>Self-employed</label>
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.fathersOccupationalStatus === 'inbetweenjobs'} />
            <label>in-between jobs</label>
          </Grid>
          <Grid item xs={12} sm={1}>
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.fathersOccupationalStatus === 'retiree'} />
            <label>retiree</label>
          </Grid>
          <Grid item xs={12} sm={7}>
            <input type="checkbox" checked={f.fathersOccupationalStatus === 'ofw'} />
            <label>OFW specify what country: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.fathersOccupationalStatus_OFWcountry || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={1}>
            Mother:
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.motherOccupationalStatus === 'workingwithinthecountry'} />
            <label>working within the country</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            <input type="checkbox" checked={f.motherOccupationalStatus === 'selfemployed'} />
            <label>Self-employed</label>
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.motherOccupationalStatus === 'inbetweenjobs'} />
            <label>in-between jobs</label>
          </Grid>
          <Grid item xs={12} sm={1}>
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.motherOccupationalStatus === 'retiree'} />
            <label>retiree</label>
          </Grid>
          <Grid item xs={12} sm={7}>
            <input type="checkbox" checked={f.motherOccupationalStatus === 'ofw'} />
            <label>OFW specify what country: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.motherOccupationalStatus_OFWcountry || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={12} marginTop={2}>
            <Typography align="left" sx={{ fontSize: 14 }}>
              PERSONAL
            </Typography>
          </Grid>
          <Grid item xs={12} sm={2}>
            Do you have?
          </Grid>
          <Grid item xs={12} sm={3}>
            <input type="checkbox" checked={f.doesHaveHearingProblem} />
            <label>hearing problems</label>
          </Grid>
          <Grid item xs={12} sm={3}>
            <input type="checkbox" checked={f.doesHaveVisionProblem} />
            <label>vision-related problems</label>
          </Grid>
          <Grid item xs={12} sm={4}>
            <input type="checkbox" checked={f.doesNotHaveHealthProblems} />
            <label>none</label>
          </Grid>
          <Grid item xs={12} sm={2}>
          </Grid>
          <Grid item xs={12} sm={10}>
            <input type="checkbox" checked={!(!f.otherHealthProblems)} />
            <label>other health problems (kindly specify) </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.otherHealthProblems || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={12}>
            <label>Goals and Ambitions: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.goalsAndAmbitions || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={12}>
            <label>Talent(s) & Hobby: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.talentsAndHobby || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={12}>
            <label>Strong points that you believe you have: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.talentsAndHobby || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={12}>
            <label>Weaknesses/Difficulties that you believe you have: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.weaknessPoints || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={12} marginTop={2}>
            <Typography align="left" sx={{ fontSize: 14 }}>
              SCHOOL
            </Typography>
          </Grid>
          <Grid item xs={12} sm={2}>
            Honor Student?
          </Grid>
          <Grid item xs={12} sm={1}>
            <input type="checkbox" checked={f.isHonorStudent} />
            <label>yes</label>
          </Grid>
          <Grid item xs={12} sm={1}>
            <input type="checkbox" checked={!f.isHonorStudent} />
            <label>no</label>
          </Grid>
          <Grid item xs={12} sm={8}>
            <label>academic rank: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.academicRank || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={6}>
            <label>Favorite Subject(s): </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.favSubj || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={6}>
            <label>Subject(s) you find difficult: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.difficultSubj || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={4}>
            Been Subjected to disciplinary action?
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.isSubjectedToDisciplinaryActions} />
            <label>yes</label>
          </Grid>
          <Grid item xs={12} sm={6}>
            <input type="checkbox" checked={!f.isSubjectedToDisciplinaryActions} />
            <label>no</label>
          </Grid>
          <Grid item xs={12} sm={1}>
          </Grid>
          <Grid item xs={12} sm={11}>
            <label>If yes, When and what offense: </label>
            <label style={{ textDecoration: 'underline' }}>
              {f.whenAndWhatOffense || '_______'}
            </label>
          </Grid>
          <Grid item xs={12} sm={12} marginTop={2}>
            <Typography align="left" sx={{ fontSize: 14 }}>
              CONCERNS/ PROBLEMS
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3}>
            Common problem encounter:
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.doesHaveFamilyProblems} />
            <label>family related</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.doesHaveSchoolProblems} />
            <label>school related</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.doesHavePeerProblems} />
            <label>peer related</label>
          </Grid>
          <Grid item xs={12} sm={2}>
            <input type="checkbox" checked={f.doesHaveSelfProblems} />
            <label>self-related</label>
          </Grid>
          <Grid item xs={12} sm={12}>
            <label>Any problem/concerns that you want to discuss with your Guidance Facilitator/Counselor</label>
          </Grid>
          <Grid item xs={12} sm={11}>
            <label style={{ textDecoration: 'underline' }}>
              {f.problemsToDiscuss || '___________________________________________'}
            </label>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}

const Div = styled.div`
    background-color: #fff;
    display: flex;
    max-width: 600px;
    width: 100%;
    max-height: 1056px;
    height: 100%;
    flex-direction: column;
    color: #000;
    /* margin: auto auto; */
    /* padding: 11px 0 80px; */
  `;

const Div2 = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    padding: 0 11px;
  `;

const Div3 = styled.div`
    gap: 2px;
    border-width: 2px;
    border-style: none;
    border-color: rgba(5, 5, 5, 1);
    justify-content: space-around;
    flex-direction: row;
    display: flex;
    @media (max-width: 991px) {
      display: flex;
    }
    @media (max-width: 640px) {
      display: flex;
    }
  `;

const Img = styled.img`
    aspect-ratio: 0.97;
    object-fit: auto;
    object-position: center;
    width: 39px;
    box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
    margin-top: 11px;
  `;

const Div4 = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    flex-basis: 0;
    width: fit-content;
    border-bottom: solid;
    border-width: 1px;
    -new-property: 5px solid red;
  `;

const Div5 = styled.div`
    display: flex;
    align-items: start;
    justify-content: space-between;
    gap: 20px;
  `;

const Div6 = styled.div`
    margin: 15px 0 0 5px;
    font: 400 4px Inter, sans-serif;
    padding-right: 15px;
    border-right: solid;
    border-width: 1px;
  `;

const Div7 = styled.div`
    display: flex;
    flex-direction: column;
    white-space: nowrap;
    flex-grow: 1;
    flex-basis: 0%;
    margin-right: -5px;
    border-width: 1px;
  `;

const Div8 = styled.div`
    text-align: center;
    align-self: end;
    color: rgba(8, 8, 8, 1);
    border-style: solid;
    border-width: 1px;
    margin-right: 5px;
    padding: 5px;
    font: 400 8px Inter, sans-serif;
  `;

const Div9 = styled.div`
    margin: 10px 0 0 58px;
    font: 600 18px Inter, sans-serif;
  `;

const Div10 = styled.div`
    margin-top: 20px;
    font: 600 14px Inter, sans-serif;
  `;

const Div11 = styled.div`
    text-align: center;
    align-self: center;
    margin-top: 20px;
    width: 324px;
    font: 600 18px Inter, sans-serif;
  `;

const Div12 = styled.div`
    display: flex;
    margin-top: 35px;
    width: 100%;
    flex-direction: column;
    padding: 0 11px;
  `;

const Div13 = styled.div`
    display: flex;
    justify-content: space-between;
    gap: 5px;
    font-size: 10px;
    text-align: center;
  `;

const Div14 = styled.div`
    display: flex;
    gap: 1px;
  `;

const Div15 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 400;
  `;

const Div16 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    text-decoration-line: underline;
    white-space: nowrap;
    flex-grow: 1;
  `;

const Div17 = styled.div`
    display: flex;
    gap: 2px;
  `;

const Div18 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 400;
    flex-grow: 1;
  `;

const Div19 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    text-decoration-line: underline;
    flex-grow: 1;
  `;

const Div20 = styled.div`
    display: flex;
    gap: 3px;
    white-space: nowrap;
  `;

const Div21 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 400;
    flex-grow: 1;
  `;

const Div22 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    letter-spacing: -0.2px;
    text-decoration-line: underline;
    flex-grow: 1;
  `;

const Div23 = styled.div`
    display: flex;
    margin-top: 8px;
    align-items: start;
    gap: 5px;
    font-size: 12px;
    text-align: center;
  `;

const Div24 = styled.div`
    display: flex;
    gap: 1px;
  `;

const Div25 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 400;
    flex-grow: 1;
  `;

const Div26 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    text-decoration-line: underline;
    flex-grow: 1;
  `;

const Div27 = styled.div`
    align-self: stretch;
    display: flex;
    gap: 3px;
    font-weight: 400;
  `;

const Div28 = styled.div`
    font-family: Inter, sans-serif;
    flex-grow: 1;
  `;

const Div29 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    text-decoration-line: underline;
  `;

const Div30 = styled.div`
    font-family: Inter, sans-serif;
  `;

const Div31 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    letter-spacing: -0.2px;
    text-decoration-line: underline;
    white-space: nowrap;
    flex-grow: 1;
  `;

const Div32 = styled.div`
    display: flex;
    gap: 1px;
    white-space: nowrap;
  `;

const Div33 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 400;
    flex-grow: 1;
  `;

const Div34 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    letter-spacing: -0.2px;
    text-decoration-line: underline;
    flex-grow: 1;
  `;

const Div35 = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    margin: 0 auto;
  `;

const Div36 = styled.div`
    text-align: center;
    align-self: center;
    margin-top: 28px;
    white-space: nowrap;
    font: 700 11px Inter, sans-serif;
  `;

const Div37 = styled.div`
    letter-spacing: -0.2px;
    margin-top: 24px;
    font: 400 10px Inter, sans-serif;
  `;

const Div38 = styled.div`
    align-self: center;
    display: flex;
    width: 100%;
    max-width: 400px;
    justify-content: space-between;
    gap: 20px;
    font-weight: 300;
    white-space: nowrap;
    text-align: center;
  `;

const Div39 = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    font-size: 7px;
  `;

const Img2 = styled.img`
    aspect-ratio: 2.56;
    object-fit: cover;
    object-position: center;
    width: 176px;
  `;

const Div40 = styled.div`
    font-family: Inter, sans-serif;
    font-style: italic;
    margin-top: 6px;
  `;

const Div41 = styled.div`
    align-self: end;
    display: flex;
    margin-top: 55px;
    flex-direction: column;
  `;

const Div42 = styled.div`
    text-decoration-line: underline;
    font: 11px Inter, sans-serif;
  `;

const Div100 = styled.div`
    text-decoration-line: underline;
    font: 11px Inter, sans-serif;
  `;

const Div43 = styled.div`
    align-self: center;
    font: italic 7px Inter, sans-serif;
  `;

const Div44 = styled.div`
    letter-spacing: -0.2px;
    position: relative;
    margin-top: 20px;
    padding-top: 29px;
    font: 400 10px Inter, sans-serif;
  `;

const Div45 = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
  `;

const Div46 = styled.div`
    display: flex;
    margin-top: 23px;
    padding-right: 21px;
    justify-content: space-between;
    gap: 20px;
    font-size: 10px;
    white-space: nowrap;
    text-align: center;
  `;

const Div47 = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    flex-basis: 0%;
    margin-left: 1px;
  `;

const Box = styled.div`
    display: flex;
    gap: 2px;
  `;


const Box2 = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    flex-basis: 0%;
  `;

const Div49 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 500;
    align-self: center;
  `;

const Div50 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    text-decoration-line: underline;
    margin-top: 13px;
  `;

const Div51 = styled.div`
    display: flex;
    flex-direction: column;
    font-weight: 300;
    flex-grow: 1;
    flex-basis: 0%;
  `;

const Div52 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 500;
  `;

const Div53 = styled.div`
    font-family: Inter, sans-serif;
    text-decoration-line: underline;
    margin-top: 13px;
  `;

const Div54 = styled.div`
    display: flex;
    flex-direction: column;
    font-weight: 300;
    flex-grow: 1;
    flex-basis: 0%;
    margin-left: 1px;
  `;

const Div146 = styled.div`
    display: flex;
    padding-right: 21px;
    justify-content: space-between;
    gap: 20px;
    font-size: 10px;
    white-space: nowrap;
    text-align: center;
  `;

const Div147 = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    flex-basis: 0%;
    margin-left: 1px;
  `;

const Box100 = styled.div`
    display: flex;
    gap: 2px;
  `;


const Box200 = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    flex-basis: 0%;
  `;


const Div150 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 300;
    text-decoration-line: underline;
    margin-top: 2px;
  `;

const Div151 = styled.div`
    display: flex;
    flex-direction: column;
    font-weight: 300;
    flex-grow: 1;
    flex-basis: 0%;
  `;


const Div153 = styled.div`
    font-family: Inter, sans-serif;
    text-decoration-line: underline;
    margin-top: 2px;
  `;

const Div154 = styled.div`
    display: flex;
    flex-direction: column;
    font-weight: 300;
    flex-grow: 1;
    flex-basis: 0%;
    margin-left: 1px;
  `;

const Div55 = styled.div`
    font-family: Inter, sans-serif;
    font-weight: 500;
  `;

const Div56 = styled.div`
    font-family: Inter, sans-serif;
    text-decoration-line: underline;
    margin-top: 13px;
  `;


const Div156 = styled.div`
    font-family: Inter, sans-serif;
    text-decoration-line: underline;
    margin-top: 2px;
  `;







