import * as React from "react";
import styled from "styled-components";
import { InterviewFormSchema } from "../../app/models/interviewFormSchema";
import { Box, Grid, Typography } from "@mui/material";
import moment from "moment";
import { apiURL } from "../../app/api/agent";

interface Props {
  f: InterviewFormSchema
}

export default function MyDocument({ f }: Props) {
  const [base64image, setBase64image] = React.useState('');

  React.useEffect(() => {
    if (f) {
      imageUrlToBase64(`${apiURL}/api/images/${f.fileString?.replace("Uploads\\", "")}`)
      .then((b64) => setBase64image(b64))
    }
  }, [f]);

  const imageUrlToBase64 = async (url: string) => {
    const data = await fetch(url);
    const blob = await data.blob();
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        const base64data = reader.result;
        resolve(base64data);
      };
      reader.onerror = reject;
    });
  };

  return (
    <div style={{ margin: 40 }}>
      <Grid container>
        <Grid item textAlign='left' sm={1}>
          <Img
            loading="lazy"
            src='images/logostpaul_png.png'
          />
        </Grid>
        <Grid item textAlign='left' sm={6}>
          <Typography align="left" fontWeight={600}>
            ST. PAUL COLLEGE OF MAKATI
          </Typography>
          <Typography align="left" fontWeight={300}>
            D.M. Rivera St., Poblacion, Makati City
          </Typography>
        </Grid>
        <Grid item textAlign='right' sm={5}>
          <Typography align="right" fontWeight={600}>
            GUI-FRM-003
          </Typography>
          <Typography align="right" fontWeight={300}>
            Guidance Center
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} alignContent='center' marginTop={4} marginBottom={2}>
          <Typography variant="h3" align="center" sx={{ fontSize: 14 }}>
            Initial Interview Form
          </Typography>
          <Typography align="center" sx={{ fontSize: 12 }}>
            School Year 2024-2025
          </Typography>
        </Grid>
        <Grid item xs={12} sm={5}>
          <Typography align="left" sx={{ fontSize: 12 }}>
            Name: {f.lastName}, {f.firstName}, {f.middleInitial}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={3}>
          <Typography align="left" sx={{ fontSize: 12 }}>
            Nickname: {f.nickname}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Typography align="left" sx={{ fontSize: 12 }}>
            Grade & Section: {f.grade} - {f.section}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={1}>
          <Typography align="left" sx={{ fontSize: 12 }}>
            Age: {f.age}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={3}>
          <Typography align="left" sx={{ fontSize: 12 }}>
            Birthday: {moment(f.birthday).format('MM/DD/YYYY')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Typography align="left" sx={{ fontSize: 12 }}>
            Religion: {f.religion}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Typography align="left" sx={{ fontSize: 12 }}>
            Citizenship: {f.citizenship}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} marginTop={2}>
          <Typography variant="h6" align='center'>
            STATEMENT OF DATA PRIVACY, CONFIDENTIALITY, AND RELEASE OF
            INFORMATION
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} marginTop={2}>
          <Typography align='left'>
            The Guidance Center adheres to strict data privacy and
            confidentiality standards. Any Information or counseling records
            that you provide are strictly confidential, except in life
            threatening situations, cases of suspected abuse, or when release is
            otherwise required by law.
            <br />
            <br />
            To protect your data privacy and your right to confidentiality, your
            written authorization is required if you want us to provide
            information about your records/counseling to another person or
            department. Information about your counseling will not appear on
            your academic record.
            <br />
            <br />
            In order to provide the best possible care, your guidance
            facilitator/counselor may consult with colleagues or other
            professionals in the field. Rest assured that no identifying
            information will be disclosed.
            <br />
            <br />
            As a minor, your parents and/or guardians have a legal right to ask
            for information and to represent their children. Please write your
            name and sign below to indicate that you have read and understood
            the above statements regarding records, data privacy, and
            confidentiality. If you have any questions, you may ask your
            guidance facilitator/counselor.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} marginTop={2} align='center'>
          <Img2
            loading="lazy"
            srcSet={base64image}
          />
          <Typography fontWeight={600}>
            {f.firstName} {f.middleInitial} {f.lastName}
          </Typography>
          <span style={{ fontSize: '10px', fontStyle: 'italic' }}>Student’s Signature over Printed Name</span>
        </Grid>
        <Grid item xs={12} sm={6} marginTop={10} align='center'>
          <Typography fontWeight={600}>
            {moment(f.dateFiled).format('MM/DD/YYYY')}
          </Typography>
          <span style={{ fontSize: '10px', fontStyle: 'italic' }}>Date</span>
        </Grid>

        <Grid item xs={12} sm={12} marginTop={2}>
          <Typography variant="h6">
            In the event that my guidance facilitator/counselor reasonably
            believes that I am in danger to myself or others, I allow my guidance
            facilitator/counselor to contact the following persons, in addition to
            the school and medical personnel.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={5} marginTop={2}>
          <Typography variant="h5" textAlign='center'>
            NAME
          </Typography>
          <Typography textAlign='center'>
            1.  {f.firstContactName}
          </Typography>
          <Typography textAlign='center'>
            2.  {f.secondContactName}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={3} marginTop={2}>
          <Typography variant="h5" textAlign='center'>
            RELATIONSHIP
          </Typography>
          <Typography textAlign='center'>
            {f.firstContactRelationship}
          </Typography>
          <Typography textAlign='center'>
            {f.secondContactRelationship}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4} marginTop={2} alignSelf='right'>
          <Typography variant="h5" textAlign='center'>
            CONTACT NUMBER(S)
          </Typography>
          <Typography textAlign='center'>
            {f.firstContactContactnumber}
          </Typography>
          <Typography textAlign='center'>
            {f.secondContactRelationship}
          </Typography>
        </Grid>
      </Grid>
      <Grid container marginTop={20}>
        <Grid item xs={12} sm={12} textAlign='right' marginRight={4}>
          <span style={{ alignItems: 'left' }}>
            GUI-FRM-003
          </span>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography align="left" fontWeight={600}>
            CONFIDENTIAL
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} marginTop={2}>
          <Typography align="left" sx={{ fontSize: 14 }}>
            FAMILY BACKGROUND
          </Typography>
        </Grid>
        <Grid item xs={12} sm={1}>
          Father:
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.isFatherLiving === 'living'} />
          <label>Living</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.isFatherLiving !== 'living'} />
          <label>Deceased</label>
        </Grid>
        <Grid item xs={12} sm={1}>
          Mother:
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.isMotherLiving === 'living'} />
          <label>Living</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.isMotherLiving !== 'living'} />
          <label>Deceased</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          Parent's Marital Status:
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.parentsMaritalStatus === 'married'} />
          <label>Married</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.parentsMaritalStatus === 'livingtogether'} />
          <label>Living Together</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          <input type="checkbox" checked={f.parentsMaritalStatus === 'singleparent'} />
          <label>Single Parent</label>
        </Grid>
        <Grid item xs={12} sm={3}></Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.parentsMaritalStatus === 'seperated'} />
          <label>Seperated</label>
        </Grid>
        <Grid item xs={12} sm={6}>
          <input type="checkbox" checked={f.parentsMaritalStatus === 'other'} />
          <label>others: </label>
          <label style={{ textDecoration: 'underline' }}>{f.otherParentsMaritalStatus || '______'}</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          Living with:
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.livingWith === 'biologicalparent'} />
          <label>Both Biological Parent</label>
        </Grid>
        <Grid item xs={12} sm={6}>
          <input type="checkbox" checked={f.livingWith === 'biologicalmotherandstepfather'} />
          <label>Biological Mother & Step Father</label>
        </Grid>
        <Grid item xs={12} sm={2}>
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.livingWith === 'biologicalfatherandstepmother'} />
          <label>Biological Father & Step Mother</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          <input type="checkbox" checked={f.livingWith === 'motheronly'} />
          <label>Mother Only</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          <input type="checkbox" checked={f.livingWith === 'fatheronly'} />
          <label>Father Only</label>
        </Grid>
        <Grid item xs={12} sm={2}>
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.livingWith === 'stepmotheronly'} />
          <label>Step Mother Only</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          <input type="checkbox" checked={f.livingWith === 'stepfatheronly'} />
          <label>Step Father Only</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.livingWith === 'separated'} />
          <label>Separated</label>
        </Grid>
        <Grid item xs={12} sm={2}>
        </Grid>
        <Grid item xs={12} sm={10}>
          <input type="checkbox" checked={f.livingWith === 'guardian'} />
          <label>Guardian (relationship): </label>
          <label style={{ textDecoration: 'underline' }}>{f.otherLivingWith || '______'}</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          Number of Siblings
        </Grid>
        <Grid item xs={12} sm={2}>
          <label style={{ textDecoration: 'underline' }}>{f.numberOfSiblingsBrother || '___'}</label>
          <label> Brother(s)</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          <label style={{ textDecoration: 'underline' }}>{f.numberOfSiblingsSister || '___'}</label>
          <label> Sister(s)</label>
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.isOnlyChild} />
          <label>none</label>
        </Grid>
        <Grid item xs={12} sm={12} marginTop={2}>
          <Typography align="left" sx={{ fontSize: 14 }}>
            Parent's Occupational Status
          </Typography>
        </Grid>
        <Grid item xs={12} sm={1}>
          Father:
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.fathersOccupationalStatus === 'workingwithinthecountry'} />
          <label>working within the country</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          <input type="checkbox" checked={f.fathersOccupationalStatus === 'selfemployed'} />
          <label>Self-employed</label>
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.fathersOccupationalStatus === 'inbetweenjobs'} />
          <label>in-between jobs</label>
        </Grid>
        <Grid item xs={12} sm={1}>
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.fathersOccupationalStatus === 'retiree'} />
          <label>retiree</label>
        </Grid>
        <Grid item xs={12} sm={7}>
          <input type="checkbox" checked={f.fathersOccupationalStatus === 'ofw'} />
          <label>OFW specify what country: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.fathersOccupationalStatus_OFWcountry || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={1}>
          Mother:
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.motherOccupationalStatus === 'workingwithinthecountry'} />
          <label>working within the country</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          <input type="checkbox" checked={f.motherOccupationalStatus === 'selfemployed'} />
          <label>Self-employed</label>
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.motherOccupationalStatus === 'inbetweenjobs'} />
          <label>in-between jobs</label>
        </Grid>
        <Grid item xs={12} sm={1}>
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.motherOccupationalStatus === 'retiree'} />
          <label>retiree</label>
        </Grid>
        <Grid item xs={12} sm={7}>
          <input type="checkbox" checked={f.motherOccupationalStatus === 'ofw'} />
          <label>OFW specify what country: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.motherOccupationalStatus_OFWcountry || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={12} marginTop={2}>
          <Typography align="left" sx={{ fontSize: 14 }}>
            PERSONAL
          </Typography>
        </Grid>
        <Grid item xs={12} sm={2}>
          Do you have?
        </Grid>
        <Grid item xs={12} sm={3}>
          <input type="checkbox" checked={f.doesHaveHearingProblem} />
          <label>hearing problems</label>
        </Grid>
        <Grid item xs={12} sm={3}>
          <input type="checkbox" checked={f.doesHaveVisionProblem} />
          <label>vision-related problems</label>
        </Grid>
        <Grid item xs={12} sm={4}>
          <input type="checkbox" checked={f.doesNotHaveHealthProblems} />
          <label>none</label>
        </Grid>
        <Grid item xs={12} sm={2}>
        </Grid>
        <Grid item xs={12} sm={10}>
          <input type="checkbox" checked={!(!f.otherHealthProblems)} />
          <label>other health problems (kindly specify) </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.otherHealthProblems || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={12}>
          <label>Goals and Ambitions: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.goalsAndAmbitions || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={12}>
          <label>Talent(s) & Hobby: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.talentsAndHobby || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={12}>
          <label>Strong points that you believe you have: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.talentsAndHobby || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={12}>
          <label>Weaknesses/Difficulties that you believe you have: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.weaknessPoints || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={12} marginTop={2}>
          <Typography align="left" sx={{ fontSize: 14 }}>
            SCHOOL
          </Typography>
        </Grid>
        <Grid item xs={12} sm={2}>
          Honor Student?
        </Grid>
        <Grid item xs={12} sm={1}>
          <input type="checkbox" checked={f.isHonorStudent} />
          <label>yes</label>
        </Grid>
        <Grid item xs={12} sm={1}>
          <input type="checkbox" checked={!f.isHonorStudent} />
          <label>no</label>
        </Grid>
        <Grid item xs={12} sm={8}>
          <label>academic rank: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.academicRank || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={6}>
          <label>Favorite Subject(s): </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.favSubj || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={6}>
          <label>Subject(s) you find difficult: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.difficultSubj || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={4}>
          Been Subjected to disciplinary action?
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.isSubjectedToDisciplinaryActions} />
          <label>yes</label>
        </Grid>
        <Grid item xs={12} sm={6}>
          <input type="checkbox" checked={!f.isSubjectedToDisciplinaryActions} />
          <label>no</label>
        </Grid>
        <Grid item xs={12} sm={1}>
        </Grid>
        <Grid item xs={12} sm={11}>
          <label>If yes, When and what offense: </label>
          <label style={{ textDecoration: 'underline' }}>
            {f.whenAndWhatOffense || '_______'}
          </label>
        </Grid>
        <Grid item xs={12} sm={12} marginTop={2}>
          <Typography align="left" sx={{ fontSize: 14 }}>
            CONCERNS/ PROBLEMS
          </Typography>
        </Grid>
        <Grid item xs={12} sm={3}>
          Common problem encounter:
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.doesHaveFamilyProblems} />
          <label>family related</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.doesHaveSchoolProblems} />
          <label>school related</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.doesHavePeerProblems} />
          <label>peer related</label>
        </Grid>
        <Grid item xs={12} sm={2}>
          <input type="checkbox" checked={f.doesHaveSelfProblems} />
          <label>self-related</label>
        </Grid>
        <Grid item xs={12} sm={12}>
          <label>Any problem/concerns that you want to discuss with your Guidance Facilitator/Counselor</label>
        </Grid>
        <Grid item xs={12} sm={11}>
          <label style={{ textDecoration: 'underline' }}>
            {f.problemsToDiscuss || '___________________________________________'}
          </label>
        </Grid>
      </Grid>
    </div>

  );
}

const Img = styled.img`
    aspect-ratio: 0.97;
    object-fit: auto;
    object-position: center;
    width: 39px;
  `;

const Img2 = styled.img`
  aspect-ratio: 2.56;
  object-fit: auto;
  object-position: center;
  width: 176px;
`;