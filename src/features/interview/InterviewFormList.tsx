import React, { useEffect, useRef, useState } from 'react';
import LoadingComponent from "../../app/layout/LoadingComponent";
import { DataGrid, GridColDef, GridEventListener, GridToolbar } from '@mui/x-data-grid';
import { useAppDispatch, useAppSelector } from '../../app/store/configureStore';
import { fetchInterviewListAsync, interviewFormSelectors } from './interviewFormSlice';
import MyDocument from './MyDocument copy';
import jsPDF from 'jspdf';
import { Box, Container, Grid, Paper, Typography } from '@mui/material';
import html2PDF from 'jspdf-html2canvas';

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 20 },
  { field: 'username', headerName: 'Username', width: 80 },
  { field: 'firstName', headerName: 'First name', width: 130 },
  { field: 'middleInitial', headerName: 'M.I', width: 10 },
  { field: 'lastName', headerName: 'Last name', width: 130 },
  {
    field: 'age',
    headerName: 'Age',
    type: 'number',
    width: 20,
  },
  { field: 'gradeSection', headerName: 'Grade & Section', width: 180 },
  { field: 'dateFiled', headerName: 'Date Filed', width: 180 },
  /* {
    field: 'fullName',
    headerName: 'Full name',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 160,
    valueGetter: (params: GridValueGetterParams) =>
      `${params.row.firstName || ''} ${params.row.lastName || ''}`,
  }, */
];



/* const stylesPDF = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  }
}); */

export default function InterviewFormList() {
  const interviewFormList = useAppSelector(interviewFormSelectors.selectAll);
  const { productsLoaded, status } = useAppSelector(state => state.catalog);
  const dispatch = useAppDispatch();
  const reportTemplateRef = useRef(null);
  const [selectedForm, setSelectedForm] = useState();

  useEffect(() => {
    if (!productsLoaded) dispatch(fetchInterviewListAsync());
  }, [dispatch, productsLoaded]);


  const handleGeneratePdf = async() => {

    const doc = new jsPDF({
      unit: 'px',
      format: 'legal',

    });
    /* const doc = new jsPDF({
      format: 'legal',
      unit: 'px',
    }); */

    // Adding the fonts.
    console.log(doc.getFontList());
    //doc.setFont('Inter-Regular', 'normal');
    doc.setFont("Times");

    /* doc.html(reportTemplateRef.current!, {
      async callback(doc) {
        //await doc.save('document');
        const blob = await doc.output('blob');
        window.open(URL.createObjectURL(blob));
      },
    }); */

    /* await doc.html(reportTemplateRef.current!, {
      callback: function (doc) {
        return doc;
      },
      width: 2000,
      windowWidth: 2000, 
          html2canvas: {
              backgroundColor: 'lightyellow',
              width: 1000
          },
      x: 10,
      y: 50,
      autoPaging: 'text'
    }); */

    const pdf = await html2PDF(reportTemplateRef.current!, {
      jsPDF: {
        format: 'letter',
      },
      imageType: 'image/jpeg',
      output: './pdf/generate.pdf'
    });

    const blob = await pdf.output('blob');
    window.open(URL.createObjectURL(blob));
  };


  const handleEvent: GridEventListener<'rowClick'> = (
    params, // GridCallbackDetails
  ) => {
    //setMessage(`Movie "${params.row.title}" clicked`);
    console.log(params.row);
    setSelectedForm(params.row);
    //handleGeneratePdf();
  };

  if (status.includes('pending')) return <LoadingComponent message="Loading List of Interview Forms..." />

  return (
    <>
      <Grid container spacing={2} marginTop={2}>
        <Grid item xs={12} sm={12} marginTop={3}>
          <DataGrid
            rows={interviewFormList}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: { page: 0, pageSize: 5 },
              },
            }}
            pageSizeOptions={[5, 10]}
            onRowClick={handleEvent}
            slots={{ toolbar: GridToolbar }}
          /* checkboxSelection */
          />
        </Grid>
        <Grid item xs={12} sm={12} marginTop={3}>
          <div>
            <button className="button" onClick={handleGeneratePdf}>
              Generate PDF
            </button>
            {selectedForm && (
              <div ref={reportTemplateRef} style={{ maxWidth: '800px', width: '100%', }}>
                <MyDocument f={selectedForm} />
              </div>
            )}
          </div>
        </Grid>
      </Grid >

    </>

  )
}
