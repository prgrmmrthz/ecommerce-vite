import React from 'react'
import { Helmet } from 'react-helmet-async'
import PageTitleWrapper from '../../components/PageTitleWrapper'
import PageTitle from '../../components/PageTitle'
import { Container, Grid, Paper } from '@mui/material'
import InterviewFormList from './InterviewFormList'

export default function InterviewFormListMain() {
  return (
    <>
    <Helmet>
      <title>Initial Interview List</title>
    </Helmet>
    <PageTitleWrapper>
      <PageTitle
        heading="GUI-FRM-003"
        subHeading="List of Initial Interview Submitted"
      />
    </PageTitleWrapper>
    <Container maxWidth="lg">
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid container item xs={12}>
          <Container component={Paper} elevation={24}>
            <InterviewFormList />
          </Container>
        </Grid>
      </Grid>
    </Container>
  </>
  )
}
