import { createAsyncThunk, createEntityAdapter, createSlice, isAnyOf } from "@reduxjs/toolkit";
import agent from "../../app/api/agent";
import { InterviewFormSchema } from "../../app/models/interviewFormSchema";
import { RootState } from "../../app/store/configureStore";

const interviewAdapter = createEntityAdapter<InterviewFormSchema>();




export const postInterviewForm = createAsyncThunk<InterviewFormSchema, InterviewFormSchema>(
    'interview/postForm',
    async (data, thunkAPI) => {
        try{
            const interviewFormCreated = await agent.InterviewForm.createForm(data);
            return interviewFormCreated;
        }catch(error: any){
            return thunkAPI.rejectWithValue({error: error.data})
        }
    }
);

export const fetchInterviewListAsync = createAsyncThunk<InterviewFormSchema[]>(
    'interview/fetchListAsync',
    async (_, thunkAPI) => {
        try {
            return await agent.InterviewForm.list();
        } catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.data});
        }
    }
)

/* export const fetchCurrentUser = createAsyncThunk<User>(
    'account/fetchCurrentUser',
    async (_, thunkAPI) => {
        thunkAPI.dispatch(setUser(JSON.parse(localStorage.getItem('user')!)));
        try{
            const user = await agent.Account.currentuser();
            localStorage.setItem('user', JSON.stringify(user));
            return user;
        }catch(error: any){
            return thunkAPI.rejectWithValue({error: error.data})
        }
    },
    {
        condition: () => {
            if (!localStorage.getItem('user')) return false;
        }
    }
); */

export const interviewFormSlice = createSlice({
    name: 'interviewFormSliceName',
    initialState : interviewAdapter.getInitialState({
        dataLoaded: false,
        status: 'idle',
    }),
    reducers: {
        /* signOut: (state) => {
            state.user = null;
            localStorage.removeItem('user');
            router.navigate('/login');
        },*/
        setForm: () => {
            //formAdapter.upsertOne(state, action.payload);
        } 
    },
    extraReducers: (builder => {
        /* builder.addCase(fetchCurrentUser.rejected, (state) => {
            state.user = null;
            localStorage.removeItem('user');
            toast.error('Session expired - please login again');
            router.navigate('/');
        }) */
        builder.addCase(fetchInterviewListAsync.pending, (state) => {
            state.status = 'pendingFetchInterviewList';
        });
        builder.addCase(fetchInterviewListAsync.fulfilled, (state, action) => {
            interviewAdapter.setAll(state, action.payload);
            state.status = 'idle';
            state.dataLoaded = true;
        });
        builder.addCase(fetchInterviewListAsync.rejected,  (state, action) => {
            console.log(action.payload);
            state.status = 'idle';
        });
        builder.addMatcher(isAnyOf(postInterviewForm.fulfilled), () =>{
            //state.iform = action.payload;
        });
        builder.addMatcher(isAnyOf(postInterviewForm.fulfilled), (_, action) =>{
            console.log(action.payload);
        });
        
    })
});

export const interviewFormSelectors = interviewAdapter.getSelectors((state: RootState) => state.interviewForm);