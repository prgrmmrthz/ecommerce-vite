import React, { useState, useRef } from 'react';
import { Box, Button, Card, CardContent, CardMedia, Checkbox, FormControlLabel, Grid, Input, MenuItem, Modal, Paper, TextField, Typography } from '@mui/material';
import { Controller, FieldValues, SubmitHandler, useForm } from 'react-hook-form';
import { LoadingButton } from '@mui/lab';
import { useAppSelector } from '../../app/store/configureStore';
import { InterviewFormSchema } from '../../app/models/interviewFormSchema';
import agent from '../../app/api/agent';
import moment from 'moment';
import Swal from 'sweetalert2';
import { router } from '../../app/router/Routes';
import SignatureCanvas from 'react-signature-canvas';
import { Height } from '@mui/icons-material';

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  Height: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function Interview2() {
  const { user } = useAppSelector(state => state.account);
  const { register, handleSubmit, control, watch, setValue, formState: { isSubmitting, errors } } = useForm();
  const selectedOption1 = watch('parentsMaritalStatus');
  const selectedOption2 = watch('livingWith');
  const selectedOptionfatherOccupation = watch('fathersOccupationalStatus');
  const selectedOptionmotherOccupation = watch('motherOccupationalStatus');
  const selectedOptionOtherhealthproblems = watch('otherhealthproblemsCB');
  const selectedOptionHonorStudent = watch('isHonorStudent');
  const selectedOptiondisciplinaryaction = watch('isSubjectedToDisciplinaryActions');
  const wFather = watch('isFatherLiving');
  const wMother = watch('isMotherLiving');
  const wOnlyChild = watch('isOnlyChild');
  const wNoProbs = watch('doesNotHaveHealthProblems');
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const sigCanvas = useRef({});

  const [previewUrl, setPreviewUrl] = useState<string | null>(null);
  const [fileSelected, setFileSelected] = useState<File>();

  if (!user) {
    router.navigate('/login');
  }

  const clear = () => sigCanvas.current.clear();

  const save = () => {
    setPreviewUrl(sigCanvas.current.getTrimmedCanvas().toDataURL("image/png"));
    const a = sigCanvas.current.getTrimmedCanvas().toDataURL("image/png");

    fetch(a)
      .then(res => res.blob())
      .then(blob => {
        const file = new File([blob], "Signature.png", { type: "image/png" });
        setFileSelected(file);
        handleClose();
        //console.log(file);
      })
  }

  const handleFileChange = (e: any) => {
    console.debug(selectedOptionOtherhealthproblems);
    // Preview the selected image
    const file = e.target.files[0];
    console.log('file selected', file);

    if (file) {
      setFileSelected(file);
      const imageUrl = URL.createObjectURL(file);
      setPreviewUrl(imageUrl);
    } else {
      setPreviewUrl(null);
    }
  }

  const onSubmit: SubmitHandler<any> = async (data: FieldValues) => {
    console.log(data);
    const { lastName,
      firstName,
      middleInitial,
      nickname,
      grade,
      section,
      age,
      birthday,
      religion,
      citizenship,
      firstContactName,
      firstContactRelationship,
      firstContactContactnumber,
      secondContactName,
      secondContactRelationship,
      secondContactContactnumber,
      parentsMaritalStatus,
      otherParentsMaritalStatus,
      livingWith,
      otherValueForLivingWith,
      isFatherLiving,
      isMotherLiving,
      isOnlyChild,
      doesHaveHearingProblem,
      doesHaveVisionProblem,
      doesNotHaveHealthProblems,
      isHonorStudent,
      isSubjectedToDisciplinaryActions,
      doesHaveFamilyProblems,
      doesHaveSchoolProblems,
      doesHavePeerProblems,
      doesHaveSelfProblems,
      numberOfSiblingsBrother,
      numberOfSiblingsSister,
      fathersOccupationalStatus,
      fathersOccupationalStatus_OFWcountry,
      motherOccupationalStatus,
      motherOccupationalStatus_OFWcountry,
      otherHealthProblems,
      goalsAndAmbitions,
      talentsAndHobby,
      strongPoints,
      weaknessPoints,
      academicRank,
      favSubj,
      difficultSubj,
      whenAndWhatOffense,
      problemsToDiscuss }
      = data;

    const a: InterviewFormSchema = {
      id: 1,
      schoolYear: '2024-2025',
      dateFiled: moment().format('YYYY-MM-DD HH:mm'),
      studentCode: user?.studentCode,
      username: `${user?.username}`,
      lastName,
      firstName,
      middleInitial,
      nickname,
      grade,
      section,
      age,
      birthday,
      religion,
      citizenship,
      firstContactName,
      firstContactRelationship,
      firstContactContactnumber,
      secondContactName: secondContactName || '',
      secondContactRelationship: secondContactRelationship || '',
      secondContactContactnumber: secondContactContactnumber || '',
      parentsMaritalStatus,
      otherParentsMaritalStatus: otherParentsMaritalStatus || '',
      livingWith,
      otherLivingWith: otherValueForLivingWith || '',
      isFatherLiving,
      isMotherLiving,
      isOnlyChild,
      doesHaveHearingProblem: doesNotHaveHealthProblems ? false : doesHaveHearingProblem,
      doesHaveVisionProblem: doesNotHaveHealthProblems ? false : doesHaveVisionProblem,
      doesNotHaveHealthProblems,
      isHonorStudent: isHonorStudent == 'yes' ? true : false,
      isSubjectedToDisciplinaryActions: isSubjectedToDisciplinaryActions == 'yes' ? true : false,
      doesHaveFamilyProblems,
      doesHaveSchoolProblems,
      doesHavePeerProblems,
      doesHaveSelfProblems,
      numberOfSiblingsBrother,
      numberOfSiblingsSister,
      fathersOccupationalStatus,
      fathersOccupationalStatus_OFWcountry: fathersOccupationalStatus_OFWcountry || '',
      motherOccupationalStatus,
      motherOccupationalStatus_OFWcountry: motherOccupationalStatus_OFWcountry || '',
      otherHealthProblems: otherHealthProblems || '',
      goalsAndAmbitions,
      talentsAndHobby,
      strongPoints,
      weaknessPoints,
      academicRank: academicRank || '',
      favSubj,
      difficultSubj,
      whenAndWhatOffense: whenAndWhatOffense || '',
      problemsToDiscuss,
      file: fileSelected
    };

    try {
      console.log('data pass as a', a);
      const response = await agent.InterviewForm.createForm(a);
      console.log('response', response);
      if (response) {
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: "Successful Form Filing",
          /* text: JSON.stringify(response), */
          showConfirmButton: false,
          timer: 1500
        });
      }
    } catch (error) {
      console.log('error', error);
      Swal.fire({
        position: "top-end",
        icon: "error",
        title: "Error Filing",
        /* text: JSON.stringify(error), */
        showConfirmButton: true
      });
    }
  };

  const getAge = (e: string) => {
    console.log(typeof e);
    var today = new Date();
    var birthDate = new Date(e);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
   setValue('age', age);
    
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <SignatureCanvas
            ref={sigCanvas}
            penColor="black"
            canvasProps={{ width: 500, height: 200, className: "sigCanvas" }}
          />
          <Button onClick={save}>Save</Button>
          <Button onClick={clear}>Clear</Button>
        </Box>
      </Modal>
      <Grid container spacing={2} sx={{ marginTop: 4 }}>
        <Grid item xs={12} sm={12}>
          <Typography fontWeight={800} align="left" gutterBottom>
            Basic Information
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Last Name"
            autoFocus
            {...register('lastName', { required: 'Last Name is required' })}
            error={!!errors.lastName}
            helperText={errors?.lastName?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="First Name"
            autoFocus
            {...register('firstName', { required: 'First Name is required' })}
            error={!!errors.firstName}
            helperText={errors?.firstName?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={1}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="M.I"
            autoFocus
            {...register('middleInitial', { required: 'Middle Initial is required' })}
            error={!!errors.middleInitial}
            helperText={errors?.middleInitial?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Nickname"
            autoFocus
            {...register('nickname', { required: 'Nickname is required' })}
            error={!!errors.nickname}
            helperText={errors?.nickname?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Grade"
            autoFocus
            {...register('grade', { required: 'This is required' })}
            error={!!errors.grade}
            helperText={errors?.grade?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Section"
            autoFocus
            {...register('section', { required: 'This is required' })}
            error={!!errors.section}
            helperText={errors?.section?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={1}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Age"
            autoFocus
            {...register('age', { required: 'Age is required' })}
            error={!!errors.age}
            helperText={errors?.age?.message as string}
            type='number'
            InputLabelProps={{ shrink: true }}
            disabled
          />
        </Grid>
        <Grid item xs={12} sm={2}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Birthday"
            autoFocus
            {...register('birthday', { required: 'Birthday is required' })}
            error={!!errors.birthday}
            helperText={errors?.birthday?.message as string}
            type='date'
            onChange={(e) => getAge(e.target.value)}
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
        <Grid item xs={12} sm={2}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Religion"
            autoFocus
            {...register('religion', /* { required: 'Religion is required' } */)}
            error={!!errors.religion}
            helperText={errors?.religion?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={2}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Citizenship"
            autoFocus
            {...register('citizenship', /* { required: 'Citizenship is required' } */)}
            error={!!errors.citizenship}
            helperText={errors?.citizenship?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography textAlign="left" gutterBottom>
            UPLOAD SIGNATURE
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4}>
          {/* <Input
            type="file"
            {...register('file', { required: true })}
            inputProps={{ accept: 'image/*' }}
            onChange={handleFileChange} /> */}
          <Button onClick={handleOpen}>Open Signature Pad</Button>
        </Grid>
        <Grid item xs={12} sm={8}>
          {/* {previewUrl && <img src={previewUrl} alt="Preview" style={{ maxWidth: '100%', marginTop: '10px' }} />} */}
          {previewUrl && (
            <Card>
              <img
                src={previewUrl}
                alt="my signature"
                style={{
                  display: "block",
                  margin: "0 auto",
                  width: "300px",
                  height: "160px"
                }}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  Signature that has been uploaded
                </Typography>
              </CardContent>
            </Card>
          )}
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography textAlign="left" gutterBottom>
            On the event that my guidance facilitator/counselor reasonably believes that I am in danger to myself or others, I allow my guidance facilitator/counselor to contact the following persons, in addidtion to the school and medical personnel.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4}>
        </Grid>
        <Grid item xs={12} sm={5}>
          <TextField
            margin="normal"
            fullWidth
            label="First Contact Name"
            autoFocus
            {...register('firstContactName', /* { required: 'This is required' } */)}
            error={!!errors.firstContactName}
            helperText={errors?.firstContactName?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <TextField
            margin="normal"
            fullWidth
            label="Relationship"
            autoFocus
            {...register('firstContactRelationship', /* { required: 'This is required' } */)}
            error={!!errors.firstContactRelationship}
            helperText={errors?.firstContactRelationship?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Contact Number(s)"
            autoFocus
            {...register('firstContactContactnumber', /* { required: 'This is required' } */)}
            error={!!errors.firstContactContactnumber}
            helperText={errors?.firstContactContactnumber?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={5}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Second Contact Name"
            autoFocus
            {...register('secondContactName', /* { required: 'This is required' } */)}
            error={!!errors.secondContactName}
            helperText={errors?.secondContactName?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Relationship"
            autoFocus
            {...register('secondContactRelationship', /* { required: 'This is required' } */)}
            error={!!errors.secondContactRelationship}
            helperText={errors?.secondContactRelationship?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Contact Number(s)"
            autoFocus
            {...register('secondContactContactnumber', /* { required: 'This is required' } */)}
            error={!!errors.secondContactContactnumber}
            helperText={errors?.secondContactContactnumber?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="h6" align="left" gutterBottom>
            FAMILY BACKGROUND
          </Typography>
        </Grid>
        <Grid item xs={12} sm={2}>
          <Controller
            name="isFatherLiving"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField
                {...field}
                select
                label="Father"
                fullWidth
              >
                <MenuItem value="living">Living</MenuItem>
                <MenuItem value="deceased">Deceased</MenuItem>
              </TextField>
            )}
          />
        </Grid>
        <Grid item xs={12} sm={2}>
          <Controller
            name="isMotherLiving"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField
                {...field}
                select
                label="Mother"
                fullWidth
              >
                <MenuItem value="living">Living</MenuItem>
                <MenuItem value="deceased">Deceased</MenuItem>
              </TextField>
            )}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <Controller
            name="parentsMaritalStatus"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField select fullWidth {...field} label="Parent's Marital Status">
                <MenuItem value="married">Married</MenuItem>
                <MenuItem value="livingtogether">Living Together</MenuItem>
                <MenuItem value="singleparent">Single Parent</MenuItem>
                <MenuItem value="seperated">Seperated</MenuItem>
                <MenuItem value="other">Other</MenuItem>
              </TextField>
            )}
          />
          {selectedOption1 === 'other' && (
            <Controller
              name="otherParentsMaritalStatus"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  style={{ marginTop: '1rem' }}
                  label="Other Value"
                />
              )}
            />
          )}
        </Grid>
        <Grid item xs={12} sm={4}>
          <Controller
            name="livingWith"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField select fullWidth {...field} label="Living With">
                <MenuItem value="biologicalparent">Both Biological Parent</MenuItem>
                <MenuItem value="biologicalmotherandstepfather">Biological Mother & Step Father</MenuItem>
                <MenuItem value="biologicalfatherandstepmother">Biological Father & Step Mother</MenuItem>
                <MenuItem value="motheronly">Mother Only</MenuItem>
                <MenuItem value="fatheronly">Father Only</MenuItem>
                <MenuItem value="stepmotheronly">Step Mother Only</MenuItem>
                <MenuItem value="stepfatheronly">Step Father Only</MenuItem>
                <MenuItem value="seperated">Seperated</MenuItem>
                <MenuItem value="guardian">Guardian</MenuItem>
              </TextField>
            )}
          />
          {selectedOption2 === 'guardian' && (
            <Controller
              name="otherValueForLivingWith"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  style={{ marginTop: '1rem' }}
                  label="Other Value"
                />
              )}
            />
          )}
        </Grid>
        <Grid item xs={12} sm={3}>
          <TextField
            margin="normal"
            required
            fullWidth
            disabled={wOnlyChild}
            label="Number of Siblings (brother)"
            autoFocus
            {...register('numberOfSiblingsBrother', /* { required: 'This is required' } */)}
            error={!!errors.numberOfSiblingsBrother}
            helperText={errors?.numberOfSiblingsBrother?.message as string}
            type='number'
            defaultValue={0}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <TextField
            margin="normal"
            required
            fullWidth
            disabled={wOnlyChild}
            label="Number of Siblings (sister)"
            autoFocus
            {...register('numberOfSiblingsSister', /* { required: 'This is required' } */)}
            error={!!errors.numberOfSiblingsSister}
            helperText={errors?.numberOfSiblingsSister?.message as string}
            type='number'
            defaultValue={0}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Controller
            name="isOnlyChild"
            control={control}
            defaultValue={false}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="Only Child"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="h6" align="left">
            PARENT'S OCCUPATIONAL STATUS
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Controller
            name="fathersOccupationalStatus"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField select fullWidth {...field}
                label="Father"
                defaultValue=""
                disabled={wFather === 'deceased'}
              >
                <MenuItem value="  "></MenuItem>
                <MenuItem value="workingwithinthecountry">working within the country</MenuItem>
                <MenuItem value="selfemployed">Self-employed</MenuItem>
                <MenuItem value="inbetweenjobs">in-between jobs</MenuItem>
                <MenuItem value="retiree">retiree</MenuItem>
                <MenuItem value="ofw">OFW</MenuItem>
              </TextField>
            )}
          />
          {selectedOptionfatherOccupation === 'ofw' && (
            <Controller
              name="fathersOccupationalStatus_OFWcountry"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  style={{ marginTop: '1rem' }}
                  label="specify what country"
                />
              )}
            />
          )}
        </Grid>
        <Grid item xs={12} sm={6}>
          <Controller
            name="motherOccupationalStatus"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField select fullWidth {...field}
                defaultValue=""
                disabled={wMother === 'deceased'}
                label="Mother">
                <MenuItem value="  "></MenuItem>
                <MenuItem value="workingwithinthecountry">working within the country</MenuItem>
                <MenuItem value="selfemployed">Self-employed</MenuItem>
                <MenuItem value="inbetweenjobs">in-between jobs</MenuItem>
                <MenuItem value="retiree">retiree</MenuItem>
                <MenuItem value="ofw">OFW</MenuItem>
              </TextField>
            )}
          />
          {selectedOptionmotherOccupation === 'ofw' && (
            <Controller
              name="motherOccupationalStatus_OFWcountry"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  style={{ marginTop: '1rem' }}
                  label="specify what country"
                />
              )}
            />
          )}
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="h6" align="left">
            PERSONAL
          </Typography>
        </Grid>
        <Grid item xs={12} sm={2}>
          <Controller
            name="doesHaveHearingProblem"
            control={control}
            defaultValue={false}
            disabled={wNoProbs}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="hearing problem"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <Controller
            name="doesHaveVisionProblem"
            control={control}
            disabled={wNoProbs}
            defaultValue={false}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="vision-related problems"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={1}>
          <Controller
            name="doesNotHaveHealthProblems"
            control={control}
            defaultValue={false}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="none"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <Controller
            name="otherhealthproblemsCB"
            control={control}
            disabled={wNoProbs}
            defaultValue={false}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="other health problems"
              />
            )}
          />
          {selectedOptionOtherhealthproblems && (
            <Controller
              name="otherHealthProblems"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  style={{ marginTop: '1rem' }}
                  label="Kindly Secify other health problems"
                />
              )}
            />
          )}
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Goals and Ambitions"
            autoFocus
            {...register('goalsAndAmbitions', /* { required: 'This is required' } */)}
            error={!!errors.goalsAndAmbitions}
            helperText={errors?.goalsAndAmbitions?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Talent(s) and Hobby"
            autoFocus
            {...register('talentsAndHobby', /* { required: 'This is required' } */)}
            error={!!errors.talentsAndHobby}
            helperText={errors?.talentsAndHobby?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Strong points that you believe you have"
            autoFocus
            {...register('strongPoints', /* { required: 'This is required' } */)}
            error={!!errors.strongPoints}
            helperText={errors?.strongPoints?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Weaknesses/Difficulties that you believe you have"
            autoFocus
            {...register('weaknessPoints', /* { required: 'This is required' } */)}
            error={!!errors.weaknessPoints}
            helperText={errors?.weaknessPoints?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="h6" align="left" gutterBottom>
            SCHOOL
          </Typography>
        </Grid>
        <Grid item xs={12} sm={2}>
          <Controller
            name="isHonorStudent"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField select fullWidth {...field}
                label="Honor Student?">
                <MenuItem value="yes">yes</MenuItem>
                <MenuItem value="no">no</MenuItem>
              </TextField>
            )}
          />
          {selectedOptionHonorStudent === 'yes' && (
            <Controller
              name="academicRank"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  style={{ marginTop: '1rem' }}
                  label="academic rank"
                />
              )}
            />
          )}
        </Grid>
        <Grid item xs={12} sm={6}>
          <Controller
            name="isSubjectedToDisciplinaryActions"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <TextField select fullWidth {...field}
                label="Been Subjected to disciplinary action?">
                <MenuItem value="yes">yes</MenuItem>
                <MenuItem value="no">no</MenuItem>
              </TextField>
            )}
          />
          {selectedOptiondisciplinaryaction === 'yes' && (
            <Controller
              name="whenAndWhatOffense"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth
                  style={{ marginTop: '1rem' }}
                  label="When and what offense"
                />
              )}
            />
          )}
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Favorite Subject(s)"
            autoFocus
            {...register('favSubj', /* { required: 'This is required' } */)}
            error={!!errors.favSubj}
            helperText={errors?.favSubj?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            margin="normal"
            required
            fullWidth
            label="Subject(s) you find difficult"
            autoFocus
            {...register('difficultSubj', /* { required: 'This is required' } */)}
            error={!!errors.difficultSubj}
            helperText={errors?.difficultSubj?.message as string}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="h6" align="left" gutterBottom>
            CONCERNS/PROBLEMS
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography align="left" gutterBottom>
            Common problem encounter
          </Typography>
        </Grid>
        <Grid item xs={12} sm={2}>
          <Controller
            name="doesHaveFamilyProblems"
            control={control}
            defaultValue={false}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="family related"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={2}>
          <Controller
            name="doesHaveSchoolProblems"
            control={control}
            defaultValue={false}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="school related"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={2}>
          <Controller
            name="doesHavePeerProblems"
            control={control}
            defaultValue={false}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="peer related"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={2}>
          <Controller
            name="doesHaveSelfProblems"
            control={control}
            defaultValue={false}
            render={({ field }) => (
              <FormControlLabel
                sx={{ fontSize: '200px' }}
                control={<Checkbox {...field} />}
                label="self-related"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography align="left" gutterBottom>
            Any problem/concerns that you want to discuss with your Guidance Facilitator/Counselor
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            placeholder="Any problem/concerns that you want to discuss with your Guidance Facilitator/Counselor"
            multiline
            style={{ width: '800px' }}
            inputProps={{ maxLength: 400 }}
            {...register('problemsToDiscuss', { required: 'This is required' })}
          />
        </Grid>
        <Grid item xs={12}>
          <LoadingButton
            loading={isSubmitting}
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Submit
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
}







