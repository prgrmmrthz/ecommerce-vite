import React from 'react'
import { Helmet } from 'react-helmet-async'
import PageTitle from '../../components/PageTitle'
import PageTitleWrapper from '../../components/PageTitleWrapper'
import { Container, Grid, Paper } from '@mui/material'
import Interview2 from './InterviewForm2'

export default function InterviewForm003() {
  return (
    <>
      <Helmet>
        <title>Forms - Components</title>
      </Helmet>
      <PageTitleWrapper>
        <PageTitle
          heading="GUI-FRM-002"
          subHeading="Student Individual Inventory Form"
        />
      </PageTitleWrapper>
      <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
          <Grid container item xs={12}>
            <Container component={Paper} elevation={24}>
              <Interview2 />
            </Container>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}
