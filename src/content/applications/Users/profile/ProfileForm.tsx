import { Button, Grid, TextField, Typography } from '@mui/material';
import React, { useEffect } from 'react';
import { useForm, SubmitHandler, FieldValues } from 'react-hook-form';
import agent from 'src/app/api/agent';
import { useAppSelector } from 'src/app/store/configureStore';

type FormData = {
    lastName: string;
    firstName: string;
    middleInitial: string;
    nickname: string;
    gradeYearLevel: string;
    section: string;
    age: number;
    birthday: string;
    placeOfBirth?: any;
    nationality?: any;
    religion: string;
    email?: any;
    address?: any;
    contactNumber: number;
    pictureString?: any;
    citizenship: string;
};

export default function ProfileForm() {
    const { user } = useAppSelector(state => state.account);
    const { register, handleSubmit, setValue } = useForm<FormData>();

    const onSubmit: SubmitHandler<any> = async (data: FieldValues) => {
        console.log(data);
    }

    useEffect(() => {
        if (user) {
            agent.Profile.details(Number(user.studentCode))
                .then(res => {
                    if (res) {
                        const a: string = res.birthday;
                        setValue('lastName', res.lastName);
                        setValue('firstName', res.firstName);
                        setValue('middleInitial', res.middleInitial);
                        setValue('nickname', res.nickname);
                        setValue('gradeYearLevel', res.gradeYearLevel);
                        setValue('section', res.section);
                        setValue('age', res.age);
                        setValue('birthday', a.substring(0, 10));
                        setValue('placeOfBirth', res.placeOfBirth);
                        setValue('nationality', res.citizenship);
                        setValue('religion', res.religion);
                        setValue('email', res.email);
                        setValue('address', res.address);
                        setValue('contactNumber', res.contactNumber);
                    }
                });
        }
    }, [user]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={1} marginLeft={2} marginTop={4}>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Last Name:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('lastName')}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        First Name:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('firstName')}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Middle Name:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('middleInitial')}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Nickame:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('nickname')}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Grade/Year Level:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('gradeYearLevel')}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Section:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('section')}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Age:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('age')}
                        type='number'
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Birthday:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('birthday')}
                        type='date'
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Place of Birth:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('placeOfBirth')}
                        type='text'
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Nationality:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('nationality')}
                        type='text'
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Religion:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('religion')}
                        type='text'
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Email:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('email')}
                        type='email'
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Address:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('address')}
                        type='text'
                    />
                </Grid>
                <Grid item xs={4}>
                    <Typography align="left" fontWeight={650}>
                        Contact Number:
                    </Typography>
                </Grid>
                <Grid item xs={7}>
                    <TextField
                        required
                        fullWidth
                        autoFocus
                        size="small"
                        {...register('contactNumber')}
                        type='number'
                    />
                </Grid>
                <Grid item xs={12}>
                    <Button type="submit" fullWidth variant="contained" color="primary">
                        Update
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
};