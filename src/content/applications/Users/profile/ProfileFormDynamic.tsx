import { Button, Grid, TextField, Typography } from '@mui/material';
import { profile } from 'console';
import React, { useEffect, useState } from 'react';
import { useForm, useFieldArray, SubmitHandler, Controller } from 'react-hook-form';
import agent from 'src/app/api/agent';
import { UserFormFields } from 'src/app/models/user';
import { useAppSelector } from 'src/app/store/configureStore';

type FormData = {
    lastName: string;
    firstName: string;
    middleInitial: string;
    nickname: string;
    gradeYearLevel: string;
    section: string;
    age: number;
    birthday: string;
    placeOfBirth?: any;
    nationality?: any;
    religion: string;
    email?: any;
    address?: any;
    contactNumber: number;
    pictureString?: any;
    citizenship: string;
};

export default function ProfileFormDynamic() {
    const { user } = useAppSelector(state => state.account);
    const [profile, setProfile] = useState<ProfileSchema | null>(null);
    const [fields, setFields] = useState<UserFormFields[]>([
        {
            name: 'lastName',
            label: 'Last Name',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'firstName',
            label: 'First Name',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'middleInitial',
            label: 'Middle Name',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'nickname',
            label: 'Nickname',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'gradeYearLevel',
            label: 'Grade/Year level',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'section',
            label: 'Section',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'age',
            label: 'Age',
            sx: 7, Lsx: 4,
            type: 'number'
        },
        {
            name: 'birthday',
            label: 'Birthday',
            sx: 7, Lsx: 4,
            type: 'date'
        },
        {
            name: 'placeOfBirth',
            label: 'Place of Birth',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'nationality',
            label: 'Nationality',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'religion',
            label: 'Religion',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'email',
            label: 'Email',
            sx: 7, Lsx: 4,
            type: 'email'
        },
        {
            name: 'address',
            label: 'Address',
            sx: 7, Lsx: 4,
            type: 'text'
        },
        {
            name: 'contactNumber',
            label: 'Contact No/s',
            sx: 7, Lsx: 4,
            type: 'number'
        },
        {
            name: 'citizenship',
            label: 'Citizenship',
            sx: 7, Lsx: 4,
            type: 'text'
        },
    ]);
    const { register, handleSubmit, control, setValue } = useForm();

    const onSubmit: SubmitHandler<any> = (data) => {
        console.log(data);
    };

    useEffect(() => {
        if (user) {
            agent.Profile.details(Number(user.studentCode))
                .then(res => setProfile(res))
                .finally(() => {
                    if(profile){
                        setValue('lastName', profile.lastName);
                    }
                });
        }
    }, [user]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={1} xs={8} marginLeft={2}>
                {fields.map((f) => (
                    <>
                        <Grid item xs={f.Lsx}>
                            <Typography align="left" fontWeight={650}>
                                {f.label}:
                            </Typography>
                        </Grid>
                        <Grid item xs={f.sx}>
                            <Controller
                                name={f.name}
                                control={control}
                                render={({ field: {value, name} }) => (
                                    <TextField
                                        required
                                        variant="outlined"
                                        size='small'
                                        fullWidth
                                        type={f.type}
                                        name={f.name}
                                        value={value}
                                    />
                                )}
                            />
                        </Grid>
                    </>


                ))}
                <Grid item xs={12}>
                    <Button type="submit" fullWidth variant="contained" color="primary">
                        Update
                    </Button>
                </Grid>
            </Grid>
        </form >
    );
};