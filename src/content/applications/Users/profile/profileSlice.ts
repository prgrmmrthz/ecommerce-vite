import { createAsyncThunk, createEntityAdapter, createSlice, isAnyOf } from "@reduxjs/toolkit";
import { RootState } from "../../app/store/configureStore";
import agent from "src/app/api/agent";

const profileAdapter = createEntityAdapter<ProfileSchema>();

export const fetchProfileDetailsAsync = createAsyncThunk<ProfileSchema[], {studentCode: number}>(
    'profile/fetchDetails',
    async ({studentCode}, thunkAPI) => {
        try {
            return await agent.Profile.details(Number(studentCode));
        } catch (error: any) {
            return thunkAPI.rejectWithValue({error: error.data});
        }
    }
)

export const profileSlice = createSlice({
    name: 'profile',
    initialState: profileAdapter.getInitialState({
        profileLoaded: false,
        status: 'idle'
    }),
    reducers: {},
    extraReducers: (builder => {
        builder.addCase(fetchProfileDetailsAsync.pending, (state) => {
            state.status = 'pendingFetchProducts';
        });
        builder.addCase(fetchProfileDetailsAsync.fulfilled, (state, action) => {
            profileAdapter.setAll(state, action.payload);
            state.status = 'idle';
            state.profileLoaded = true;
        });
        builder.addCase(fetchProfileDetailsAsync.rejected,  (state, action) => {
            console.log(action.payload);
            state.status = 'idle';
        });
    })
})