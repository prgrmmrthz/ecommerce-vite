import { Helmet } from 'react-helmet-async';
import Footer from 'src/components/Footer';
import { Grid, Container } from '@mui/material';
import ProfileCover from './ProfileCover';
import React from 'react';
import { useAppSelector } from 'src/app/store/configureStore';
import ProfileForm from './ProfileForm';

function ManagementUserProfile() {
  const { user } = useAppSelector(state => state.account);

  const user2 = {
    savedCards: 7,
    name: user?.fullName,
    coverImg: 'https://media.istockphoto.com/id/1499066806/photo/yellow-question-mark-standing-out-from-the-crowd.jpg?s=1024x1024&w=is&k=20&c=UEQpLhubYDATrrOuDg2CoMJZzLnf8toFao421FUKsT4=',
    avatar: 'images/logostpaul_png.png',
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage",
    nikcName: 'Web Developer',
    gradeSection: 'Barcelona, Spain',
    birthday: '465'
  };

  return (
    <>
      <Helmet>
        <title>User Details - Management</title>
      </Helmet>
      <Container sx={{ mt: 3 }} maxWidth="lg">
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="stretch"
            spacing={3}
            marginBottom={2}
          >
            <Grid item xs={12} md={12}>
              <ProfileCover user={user2} />
            </Grid>
          </Grid>
      </Container>
      <Footer />
    </>
  );
}

export default ManagementUserProfile;
