import PropTypes from 'prop-types';
import {
  Box,
  Typography,
  Card,
  Tooltip,
  Avatar,
  CardMedia,
  Button,
  IconButton,
  Grid,
  TextField,
  MenuItem
} from '@mui/material';
import { styled } from '@mui/material/styles';

import ArrowBackTwoToneIcon from '@mui/icons-material/ArrowBackTwoTone';
import UploadTwoToneIcon from '@mui/icons-material/UploadTwoTone';
import React, { useEffect, useState } from 'react';
import { useAppSelector } from 'src/app/store/configureStore';
import { FieldValues, SubmitHandler, useForm, Controller } from 'react-hook-form';
import agent, { apiURL } from 'src/app/api/agent';
import Swal from 'sweetalert2';
import { LoadingButton } from '@mui/lab';

type FormData = {
  lastName: string;
  firstName: string;
  middleInitial: string;
  nickname: string;
  gradeYearLevel: string;
  section: string;
  age: number;
  birthday: string;
  placeOfBirth?: any;
  nationality?: any;
  religion: string;
  email?: any;
  address?: any;
  contactNumber: number;
  pictureString?: any;
  citizenship: string;
  gender: string;
  academicYear: string;
};

const Input = styled('input')({
  display: 'none'
});

const AvatarWrapper = styled(Card)(
  ({ theme }) => `

    position: relative;
    overflow: visible;
    display: inline-block;
    margin-top: -${theme.spacing(9)};
    margin-left: ${theme.spacing(2)};

    .MuiAvatar-root {
      width: ${theme.spacing(16)};
      height: ${theme.spacing(16)};
    }
`
);

const ButtonUploadWrapper = styled(Box)(
  ({ theme }) => `
    position: absolute;
    width: ${theme.spacing(4)};
    height: ${theme.spacing(4)};
    bottom: -${theme.spacing(1)};
    right: -${theme.spacing(1)};

    .MuiIconButton-root {
      border-radius: 100%;
      background: ${theme.colors.primary.main};
      color: ${theme.palette.primary.contrastText};
      box-shadow: ${theme.colors.shadows.primary};
      width: ${theme.spacing(4)};
      height: ${theme.spacing(4)};
      padding: 0;
  
      &:hover {
        background: ${theme.colors.primary.dark};
      }
    }
`
);

const CardCover = styled(Card)(
  ({ theme }) => `
    position: relative;

    .MuiCardMedia-root {
      height: ${theme.spacing(26)};
    }
`
);

const CardCoverAction = styled(Box)(
  ({ theme }) => `
    position: absolute;
    right: ${theme.spacing(2)};
    bottom: ${theme.spacing(2)};
`
);

interface ProfileForm {
  id: number;
  lastName: string;
  firstName: string;
  middleInitial: string;
  nickname: string;
  gradeYearLevel: string;
  section: string;
  age: number;
  birthday: string;
  placeOfBirth: string;
  nationality: string;
  religion: string;
  email: string;
  address: string;
  contactNumber: number;
  studentCode: number;
  studPic?: File;
  gender: string;
  academicYear: string;
}

const ProfileCover = () => {
  const { user } = useAppSelector(state => state.account);
  const [previewUrl, setPreviewUrl] = useState<string>();
  const [fileSelected, setFileSelected] = useState<File>();
  const { register, handleSubmit, setValue, control, formState: { isSubmitting, isDirty, isValid } } = useForm<FormData>();
  const [profileId, setProfileId] = useState<number>(0);

  useEffect(() => {
    if (user) {
      agent.Profile.details(Number(user.studentCode))
        .then(res => {
          if (res) {
            setProfileId(res.id);
            const a: string = res.birthday;
            setValue('lastName', res.lastName);
            setValue('firstName', res.firstName);
            setValue('middleInitial', res.middleInitial);
            setValue('nickname', res.nickname);
            setValue('gradeYearLevel', res.gradeYearLevel);
            setValue('section', res.section);
            setValue('age', res.age);
            setValue('birthday', a.substring(0, 10));
            setValue('placeOfBirth', res.placeOfBirth);
            setValue('nationality', res.nationality);
            setValue('religion', res.religion);
            setValue('gender', res.gender);
            setValue('email', res.email);
            setValue('address', res.address);
            setValue('contactNumber', res.contactNumber);
            setValue('academicYear', res.academicYear);
            if (res.pictureString) {
              setPreviewUrl(`${apiURL}/api/images/${res.pictureString?.replace("Uploads\\", "")}`);
            }

          }
        });
    }
  }, [user]);

  const onSubmit: SubmitHandler<any> = async (d: FieldValues) => {
    console.log(d);
    if (user) {
      const a: ProfileForm = {
        id: profileId,
        lastName: d.lastName,
        firstName: d.firstName,
        middleInitial: d.middleInitial,
        nickname: d.nickname,
        gradeYearLevel: d.gradeYearLevel,
        section: d.section,
        age: d.age,
        birthday: d.birthday,
        placeOfBirth: d.placeOfBirth,
        nationality: d.nationality,
        religion: d.religion,
        email: d.email,
        address: d.address,
        contactNumber: d.contactNumber,
        studentCode: user?.studentCode,
        studPic: fileSelected,
        gender: d.gender,
        academicYear: d.academicYear
      }//a

      try {
        console.log('data pass as a', a);
        const response = await agent.Profile.updateProfile(a);
        console.log('response', response);
        if (response) {
          Swal.fire({
            position: "top-end",
            icon: "success",
            title: "Successful Updating the Profile",
            /* text: JSON.stringify(response), */
            showConfirmButton: false,
            timer: 1500
          });
        }
      } catch (error) {
        console.log('error', JSON.stringify(error));
        Swal.fire({
          position: "top-end",
          icon: "error",
          title: "Error Filing",
          /* text: JSON.stringify(error), */
          showConfirmButton: true
        });
      }//tryCatch

    }
  }//onSubmit

  const handleFileChange = (e: any) => {
    // Preview the selected image
    const file = e.target.files[0];
    //console.log('file selected', file);

    if (file) {
      setFileSelected(file);
      const imageUrl = URL.createObjectURL(file);
      setPreviewUrl(imageUrl);
    }
  }

  return (
    <>
      <Box display="flex" mb={3}>
        <Tooltip arrow placement="top" title="Go back">
          <IconButton color="primary" sx={{ p: 2, mr: 2 }}>
            <ArrowBackTwoToneIcon />
          </IconButton>
        </Tooltip>
        <Box>
          <Typography variant="h3" component="h3" gutterBottom>
            Profile for {user?.fullName}
          </Typography>
          <Typography variant="subtitle2">
            This is a profile page. Easy to modify, always blazing fast
          </Typography>
        </Box>
      </Box>
      <CardCover>
        <CardMedia image='https://media.istockphoto.com/id/1499066806/photo/yellow-question-mark-standing-out-from-the-crowd.jpg?s=1024x1024&w=is&k=20&c=UEQpLhubYDATrrOuDg2CoMJZzLnf8toFao421FUKsT4=' />
        <CardCoverAction>
          <Input accept="image/*" id="change-cover" multiple type="file" />
          <label htmlFor="change-cover">
            <Button
              startIcon={<UploadTwoToneIcon />}
              variant="contained"
              component="span"
            >
              Change cover
            </Button>
          </label>
        </CardCoverAction>
      </CardCover>
      <AvatarWrapper>
        <Avatar variant="rounded" alt={user?.fullName} src={previewUrl} />
        <ButtonUploadWrapper>
          <Input
            accept="image/*"
            id="icon-button-file"
            name="icon-button-file"
            type="file"
            onChange={handleFileChange}
          />
          <label htmlFor="icon-button-file">
            <IconButton component="span" color="primary">
              <UploadTwoToneIcon />
            </IconButton>
          </label>
        </ButtonUploadWrapper>
      </AvatarWrapper>

      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={1} marginLeft={2} marginTop={4}>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Last Name:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('lastName')}
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              First Name:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('firstName')}
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Middle Name:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('middleInitial')}
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Nickame:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('nickname')}
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Grade/Year Level:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('gradeYearLevel')}
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Section:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('section')}
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Age:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('age')}
              type='number'
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Birthday:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('birthday')}
              type='date'
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Place of Birth:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('placeOfBirth')}
              type='text'
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Nationality:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('nationality')}
              type='text'
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Religion:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('religion')}
              type='text'
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Email:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('email')}
              type='email'
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Address:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('address')}
              type='text'
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Contact Number:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('contactNumber')}
              type='number'
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Academic Year:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <TextField
              required
              fullWidth
              autoFocus
              size="small"
              {...register('academicYear')}
            />
          </Grid>
          <Grid item xs={4}>
            <Typography align="left" fontWeight={650}>
              Gender:
            </Typography>
          </Grid>
          <Grid item xs={7}>
            <Controller
              name="gender"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <TextField
                  {...field}
                  select
                  label="Gender"
                  fullWidth
                  size="small"

                >
                  <MenuItem value="male">Male</MenuItem>
                  <MenuItem value="female">Female</MenuItem>
                </TextField>
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <LoadingButton loading={isSubmitting} type="submit" fullWidth variant="contained" color="primary" disabled={!isDirty}>
              Update
            </LoadingButton>
          </Grid>
        </Grid>
      </form >
    </>
  );
};

export default ProfileCover;
